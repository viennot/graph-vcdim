#include <cassert>
#include <algorithm>

#include "distrib.hh"

distrib::distrib(size_t size, std::function<uint(size_t)> make) : elts() {
    elts.reserve(size);
    for(size_t i = size_t(0); i < size; ++i) {
        elts.push_back(make(i));
    }
    std::sort(elts.begin(), elts.end());
}

void distrib::print(std::ostream & os, std::vector<uint> perct) {

    size_t sz = elts.size();
    if (sz == 0) {
        os <<"("<< sz <<" elts)\n";
        return;
    }
    
    os << "min: "<< elts[0];
    size_t prev_i = 0;
    for(size_t ip = 0; ip < perct.size(); ++ip) {
        size_t p = perct[ip];
        size_t i = p * sz / 100;
        if (i > prev_i) {
            if (elts[i] == elts[prev_i]) {
                while (i+1 < sz && elts[i+1] == elts[prev_i]) ++i;
                p = 100 * i / sz;
            } else {
                size_t next_i = (ip+1 < perct.size())
                    ? perct[ip+1] * sz / 100
                    : elts.size()-1;
                if (elts[i] == elts[next_i]) {
                    while(i > 0 && elts[i-1] == elts[next_i]) --i;
                    p = 100 * i / sz;
                }
                
            }
            os <<" "<< p <<"%: " << elts[i];
            prev_i = i;
        }
    }
    os <<" max: "<< elts[sz-1]
       <<" ("<< sz <<" elts)\n";

}
