#pragma once

#include "set_system.hh"
#include "graph.hh"
#include "partition.hh"

using mask = std::size_t; // We can compute VCdim up to word size.

struct shat_set_table {
    std::vector<uint> table; // table[i] counts the number of neighborhoods separating subset i of elts from other (i.e. having trace i on elts), where i is a bit mask coding a subset of elts, jth lowest bit coding the presence of elts[j].
    shat_set_table(uint n) : table(1, n) {} // Prepared for empty set.
    shat_set_table(const shat_set_table & sh) // Prepared for adding an element.
        : table() {
        table.reserve(2*sh.table.size());
        table.insert(table.end(), sh.table.begin(), sh.table.end());
        table.insert(table.end(), sh.table.size(), 0);
    }
};

enum ordering { ord_kcore = 1, ord_degree = 2, ord_locub = 3, ord_random = 4, ord_degree_dec = 5 }; // order high deg nodes according to a kcore elimination, non-decr degree, non-decr local ub

class vcdim {
    
    graph g;
    std::vector<node> mapping_to_orig; // Maps a node in the current graph to a node in the original graph.
    partition part; // User partition refinement for finding local bounds.
    set_elts shat_set; // Current shattered set explored (top of the stack).
    shat_set_table empty_shat_set; // Table corresponding to an empty shat set.
    std::vector<uint> ball_size; // Size of ball of radius 2 of a node.
    std::vector<uint> local_ub; // ub of a shattered set included in S = N[x].
            /* Note: An upper bound of any shattered set included in a set S of
               nodes can be obtained by partition refinement:
               Starting from the partition { V } where V is the set of nodes 
               of g, split each part P into P \cap N[y] and P - N[y] for
               each y in S. A part P then corresponds to the class of nodes u
               with same trace N[u] \cap S on S. If S contains a shattered S',
               then for each Y \subseteq S', there must exist at least one non
               empty part P of nodes with trace Y on S'. We thus have 
               2^|S'| <= p where p is the number of parts, implying 
               |S'| <= log_2(p). Note also that S is shattered if |S|=log_2(p).
             */
    set_system local_ub_inter; // given an ordering of nodes, for each node x, upper bounds on any shattered set included in the first i elements of N[x].
    std::vector<uint> local_ub_cont; // ub of a shattered set containing x.
        /* Note: If a shattered set S contains x, for each subset Y of S-{x},
           there is a closed neighborhood N[v_Y] with trace Y U {x} on S.
           As each v_Y must be in N[x], the number of subsets Y is bounded by
           |N[x]|=deg(x)+1, and S-{x} has size at most log_2(deg(x)+1),
           implying |S| <= 1 + log_2(deg(x)+1).
         */
    std::vector<uint> local_lb; // Lower bound of the maximum size of a shattered set included in N[x].
    std::vector<node> x_ball; // Store the ball around a node.
    std::vector<uint> deg_ub_count; // count of nodes x with a certain ub of shattered sets containing x (according to the degree d of x, ub is 1+log_2(d+1))
    std::vector<node> nodes; // Nodes that can be in a large shattered set.
    std::vector<uint> rank; // Rank of a node according to processing ordering (the simplest ordering is degree based).
    std::vector<bool> in_nodes; // Listed in [nodes].
    std::vector<mask> node_mask; // Mask of a node according to its trace on the shattered currently explored.
    uint lower_bound, upper_bound; // Bounds on VC dim
    std::vector<uint> nb_visits; // count visits to a node
    uint64_t nb_shattered; // count total number of shattered sets visited
    uint64_t nb_visits_total; // count total number of node visits

public:
    
    vcdim(const graph & gr, uint known_lb=0) ;
    uint compute_vcdim(ordering ord=ord_kcore, // see ordering enum above
                       bool kcore_ubs = false, // ub according to glob kcore ord
                       bool local_ubs = false, // use local ubs to decr ub
                       bool reduce = true, // reduce graph w.r.t. high deg
                       bool sparse = false, // sparse pass first
                       bool ball2 = true, // restrict search to B(x_1,2)
                       uint maxvisits = 64, // param for greedy_vcdim_lb()
                       bool just_overhead = false // stop before main loop
                       ) ;
    uint compute_vcdim_loc() ;
    uint greedy_vcdim_lb(const uint maxvisits = 64) ;
    
    std::vector<uint> kcore_elim_ub(const std::vector<uint> & rank) ; // compute local_ub_cont bounds based on a kcore like elimination according to ordering given by [rank].

    void reduce_graph(uint lb=0) ; // Reduce graph to high deg. nodes (i.e. deg.+1 >= 2^lb) plus a minimal set of nodes preserving traces on high deg. nodes.

    const graph & get_graph() const { return g; }

    uint64_t get_nb_shattered() const { return nb_shattered; }
    uint64_t get_nb_visits_total() const { return nb_visits_total; }
    
    // Compute the number of shattered sets among nodes with degree+1 >= 2^lb.
    uint64_t compute_nb_shattered(uint lb);

    void init_bounds(bool print=false) ; // rather slow upper bounds

private:

    // Init local bounds as well as globals.
    void init_ub() ; // quick upper bounds

    // Dfs traversal of shattered sets containing node *x_ball_it in x_ball which is supposed to contain a list of candidate nodes.
    uint compute_shattered_with
      (uint loc_ub,
       std::vector<node>::iterator x_ball_it, //cur. node in array of candidates
       std::vector<node>::iterator x_ball_end, // end of array
       const shat_set_table & sh_par, // table of current shat set
       uint size) ;

    // Same as previous, but visits all shattered sets instead of targetting only those with size greater than lower_bound.
    uint compute_shattered_all
      (std::vector<node>::iterator x_ball_it, //cur. node in array of candidates
       std::vector<node>::iterator x_ball_end, // end of array
       const shat_set_table & sh_par, // table of current shat set
       uint size) ;

    // Partial Dfs traversal in order to find quickly a large shattered set. A node is visited at most maxvisits, and only next maxvisits/2 nodes (by non-incr degree order) are considered for growing a shattered set.
    uint compute_shattered_greedy(const uint maxvisits,
                                  uint xi,
                                  const shat_set_table & sh_par,
                                  uint size) ;
    
    void compute_local_upper_bounds(uint nb_opt_max = 0) ; // local_ub when some nodes have been scanned (all the shattered sets containing them have been computed and can be ignored).

    // Compute local upper bounds on the size of shattered set included in the first i elements of set and store it in partial_ub, returns a lower bound of the maximum size of a shattered set included in set, the upper bound for set.size() elements, and the size of the neighborhood N[set]=U_{x in set} N[x] of set.
    std::tuple<uint, uint, uint>  local_bounds(const std::vector<node> & set,
                                               std::vector<uint> & partial_ub,
                                               uint nb_opt_max=3) ; 
    
    bool update_lower_bound(uint lb) ; // update lower_bound with lb if higher, returns true if lower_bound was indeed updated to lb
    
    uint update_upper_bound() ; // update upper_bound according to the maximum of local upper bounds, returns the number of nodes with local_ub equal to upper_bound

    void sort_nodes_by_ub_score() ; // put first nodes that make many local_ub go down when removed

    std::vector<uint> sort_kcore_ub() ; // compute kcore elimination ordering based on getting good local_ub_cont bounds which are returned.


    void sparse_ub_cont(uint incr, bool ball2) ; // Compute the exact local_ub_cont of a high degree node every incr nodes.
};

void progress(uint xi, size_t sz, uint incr) ; // printerr progress [i/sz]

void vcdim_unit() ;

uint vcdim_lower_bound(const graph& g, const bool exact_vcdim = false) ;


