#include <sstream>

#include "set_system.hh"


set_system set_system::dual() const {
    set_system h;
    h.add_elt(nb_sets() - 1);
    h.add_set(nb_elts() - 1);
    for (set s : sets()) {
        for (elt e : setelts[s]) {
            h.add(e, s); // add s to hyperedge e
        }
    }
    return h;
}

set_system set_system::power_set(uint n) {
    set_system h;
    h.add_set(0); // empty set
    for (elt e = 0; e < n; ++e) {
        for (set s : h.sets()) {
            set s_e = h.new_set();
            h.append(s_e, h[s]);
            h.add(s_e, e);
        }
    }
    return h;
}


std::ostream& operator<<(std::ostream & os, const set_elts & se) {
    os << "{";
    bool first = true;
    for (elt e : se) {
        if (first) { os << " "; first = false; }
        else { os << " "; }
        os << e;
    }
    if (first) { os << "}"; } else { os << " }"; }
    return os;
}

std::ostream& operator<<(std::ostream & os, const set_system & h) {
    os << "{\n";
    for (set s : h.sets()) {
        os << "  " << s << " : " << (h[s]) << std::endl;
    }
    os << "}\n";
    return os;
}

std::istream& operator>>(std::istream & iss, set_elts & se) {
    std::string elt_s, line;
    const elt elt_max = std::numeric_limits<elt>::max();
    
    iss >> std::ws;
    while ( ! iss.eof()) {
        iss >> elt_s >> std::ws;
        auto elt_i = std::stoll(elt_s);
        assert(elt_i >= 0 && std::uint64_t(elt_i) <= std::uint64_t(elt_max));
        se.push_back((elt)elt_i);
    }
    return iss;
}

std::istream& operator>>(std::istream & is, set_system & h) {
    while ( ! is.eof()) {
        is >> std::ws; // white space
        if (is.eof()) return is;
        std::string line;
        std::getline(is, line);
        while (line.size() > 0 && line[0] == '#') { // comment
            if (is.eof()) return is;
            std::getline(is, line);
        }
        std::istringstream iss(line);
        set s = h.new_set();
        iss >> h[s];
        for (elt e : h[s]) { h.add_elt(e); }
    }
    return is;
}

void set_system::unit() {
    std::cout <<"------------------- set_system::unit --------------------\n";
    
    set_system h;
 
    h.add(0, 1); // add to set 0 the element 1
    h.add(0, 2); 
    h.add(1, 2);
    
    for (set s : h.sets()) {
        std::cout << s <<" : { ";
        for (elt e : h[s]) { std::cout << e <<" "; } 
        std::cout <<"}\n";
    }

    for (set_elts se : h) {
        std::cout << se <<std::endl;
    }

    std::cout << h.dual();

    std::istringstream inp("# a set system:\n"
                           "2 1\n 2 3\n 2 4\n"
                           "3 2\n 3 3\n"
                           );
    inp >> h;

    std::cout << h;

    set_system all = set_system::power_set(4);
    std::cout << all.dual();
    assert(all.nb_sets() == 16);
}
