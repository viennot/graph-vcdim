#include <cassert>
#include <cstdlib>
#include <ctime>
#include <fstream>
#include <utility>
#include <algorithm>
#include <queue>

#include "integers.hh"
#include "vcdim.hh"
#include "distrib.hh"


vcdim::vcdim(const graph & gr, uint known_lb)
        : g(gr),
          mapping_to_orig(gr.nb_nodes(), node(-1)),
          part(gr.nb_nodes()),
          shat_set(),
          empty_shat_set(gr.nb_nodes()),
          ball_size(gr.nb_nodes()),
          local_ub(gr.nb_nodes(), log_2(gr.nb_nodes())),
          local_ub_inter(),
          local_ub_cont(gr.nb_nodes(), log_2(gr.nb_nodes())),
          local_lb(gr.nb_nodes(), 0),
          x_ball(gr.nb_nodes()),
          deg_ub_count(8*sizeof(mask), 0),
          nodes(),
          rank(gr.nb_nodes(), gr.nb_nodes()),
          in_nodes(gr.nb_nodes(), false),
          node_mask(gr.nb_nodes(), 0),
          lower_bound(known_lb),
          upper_bound(log_2(g.nb_nodes())),
          nb_visits(g.nb_nodes(), 0),
          nb_shattered(0),
          nb_visits_total(0)
{
    for (node u : g.nodes()) { mapping_to_orig[u] = u; } // identity
    shat_set.reserve(8*sizeof(mask));
    local_ub_inter.add_set(g.nb_nodes() - 1);
    nodes.reserve(g.nb_nodes());
    init_ub();
}

void vcdim::init_ub() {

    upper_bound = 0;
    for (node u : g.nodes()) {
        // upper bound on any shattered set containing u:
        local_ub_cont[u]  = 1 + log_2(g.degree(u) + 1); //used in local_bounds()
        upper_bound = std::max(upper_bound, local_ub_cont[u]);
    }
    std::cerr <<"lb="<< lower_bound <<" ub="<< upper_bound <<"\n";

    std::cerr <<"  local_ub_cont: ";
    distrib(g.nb_nodes(),
            [this](size_t i) { return local_ub_cont[i]; }).print(std::cerr);
    
}

void vcdim::init_bounds(bool print) {

    uint loc_ub_max = 0;

    for (node u : g.nodes()) {

        // sort N[u] by decr degree:
        x_ball.clear();
        for (node v : g.neighbors(u)) { x_ball.push_back(v); }
        std::sort(x_ball.begin(), x_ball.end(),
                  [this](const node u, const node v) {
                      return g.degree(u) > g.degree(v); // high deg first
                  });

        // bound size of shattered sets included in N[u]:
        auto [lb, ub, bsize] = local_bounds(x_ball, local_ub_inter[u], 0);
        local_lb[u] = std::max(lb, local_lb[u]);
        local_ub[u] = std::min(ub, local_ub[u]);
        ball_size[u] = bsize; // size of the ball B(u,2) of radius 2

        if (local_ub[u] > loc_ub_max) {
            loc_ub_max = local_ub[u];
            std::cerr <<"lb="<< lower_bound<<" loc_ub_max="<< loc_ub_max <<"\n";
        }

    }

    uint deg_ub = upper_bound;
    std::cerr << "degree_ub="<< upper_bound <<"\n";
    std::cerr << "local_ub="<< loc_ub_max <<"\n";
    assert(loc_ub_max <= upper_bound);
    upper_bound = loc_ub_max;    
    uint degen = g.degeneracy();
    std::cerr <<"  degeneracy: "<< degen <<"\n";
    std::cerr << "degen_ub="<< degen + 1 <<"\n";
    if (degen + 1 < upper_bound) {
        upper_bound = degen + 1;
    }
    std::cerr <<"lb="<< lower_bound <<" ub="<< upper_bound <<"\n";
        
    // Only high deg nodes can be in a large shattered set:
    nodes.clear();
    for (node u : g.nodes()) {
        if (local_ub_cont[u] > lower_bound) {
            nodes.push_back(u);
            in_nodes[u] = true;
        }
    }
    
    std::cerr <<"  local_lb: ";
    distrib(g.nb_nodes(),
            [this](size_t i) { return local_lb[i]; }).print(std::cerr);
    std::cerr <<"  local_ub: ";
    distrib(g.nb_nodes(),
            [this](size_t i) { return local_ub[i]; }).print(std::cerr);
    std::cerr <<"  local_ub_cont: ";
    distrib(g.nb_nodes(),
            [this](size_t i) { return local_ub_cont[i]; }).print(std::cerr);

    std::vector<uint> high_deg_ubs;
    for (node u : g.nodes()) {
        if (local_ub_cont[u] > lower_bound) {
            high_deg_ubs.push_back(local_ub_cont[u]);
        }
    }
    std::cerr <<"  local_ub_cont high deg only: ";
    distrib(high_deg_ubs).print(std::cerr);
    
    std::cerr << nodes.size() <<" nodes with degree >= 2^lb-1 = "
              << ((1 << lower_bound) - 1) <<"\n";

    if (print) {
        std::cout<< lower_bound
                 <<" "<< loc_ub_max
                 <<" "<< deg_ub
                 <<" "<< log_2(g.nb_nodes())
                 <<" "<< degen+1
                 <<"\n";
    }

}

uint vcdim::greedy_vcdim_lb(const uint maxvisits) {
    nodes.clear();
    for (node u : g.nodes()) {
        nodes.push_back(u);
        nb_visits[u] = 0;
    }
    // order nodes by decr deg
     std::sort(nodes.begin(), nodes.end(),
              [this](const node u, const node v) {
                   return g.degree(u) > g.degree(v);
               });
     for (uint xi = 0; xi < nodes.size(); ++xi) {
         compute_shattered_greedy(maxvisits, xi, empty_shat_set, 0);
     }
     std::cerr << "maxvisits=" << maxvisits
               <<" gives lb="<< lower_bound <<"\n";
     return lower_bound;
}

uint vcdim::compute_shattered_greedy(const uint maxvisits,
                                        uint xi,
                                        const shat_set_table & sh_par,
                                        uint size){
    node x = nodes[xi];
    if (nb_visits[x] >= maxvisits) return lower_bound; // too many visits
    ++(nb_visits[x]);
    if (local_ub_cont[x] <= lower_bound) return lower_bound;
    if (g.degree(x) + 1 < sh_par.table.size()) return lower_bound; // not shat
    shat_set_table sh(sh_par);
    std::vector<uint> & table = sh.table;
    assert(size + 1 < 8*sizeof(mask)); // mask overflow, vcdim is too high
    mask mask_x = mask(1) << size;
    bool continue_growing = true;
    uint min_size_part = 1 << (lower_bound + 1 - size - 1); // minimum size of parts to allow to grow to a shattered set of size lower_bound+1 or more
    for (node u : g.neighbors(x)) {
        mask y_u = node_mask[u]; // trace of u on parent shat set (without x)
        assert(table[y_u] > 0);
        table[y_u] -= 1; // was counted before adding x
        if (table[y_u] < min_size_part) {
            return lower_bound; // cannot grow > lower_bound
        } else if (table[y_u] <= 1) {
            continue_growing = false; // cannot split into two classes
        }
        y_u ^= mask_x;      // trace with x
        table[y_u] += 1; // one more neighbor with trace y_u
    }
    for (mask y = mask_x; y < (mask_x << 1); ++y) {
        if (table[y] < min_size_part) {
            return lower_bound; // cannot grow > lower_bound
        } else if (table[y] <= 1) {
            continue_growing = false; // cannot split into two classes
        }
    }
    shat_set.push_back(x);
    ++size;
    if (update_lower_bound(size)) {
        std::cerr << shat_set <<" is shattered\n";
    }
    uint max_shat = size;
    if (continue_growing) {
        for (node y : g.neighbors(x)) node_mask[y] ^= mask_x; // update traces
        for (uint yi = xi+1;
             yi < xi + std::max(uint(4),maxvisits/2) // try only following high deg nodes
                 && yi < nodes.size();
             ++yi) {
            max_shat = compute_shattered_greedy(maxvisits, yi, sh, size);
        }
        for (node y : g.neighbors(x)) node_mask[y] ^= mask_x; // restore traces
    }
    shat_set.pop_back();
    return max_shat;
}


uint vcdim::compute_vcdim(ordering ord,
                          bool kcore_ubs,
                          bool local_ubs,
                          bool reduce,
                          bool sparse,
                          bool ball2,
                          uint maxvisits,
                          bool just_overhead
                          ) {

    std::cerr <<"Params: ordering="<< ord
              <<" kcore_ubs="<< kcore_ubs
              <<" local_ubs="<< local_ubs
              <<" reduce="<< reduce
              <<" sparse="<< sparse
              <<" ball2="<< ball2
              <<" maxvisits="<< maxvisits
              <<"\n";

    // ------- Get local_ub_cont, upper_bound and pretty good lower_bound ---
    
    init_ub();
    greedy_vcdim_lb(maxvisits);
    
    
    // ----- Reduce graph to high deg. nodes while preserving traces ------

    if (reduce) {
        reduce_graph();
    }
    
    // --------- Only high deg nodes can be part of a large shattered set ---

    nodes.clear();
    std::vector<uint> high_deg_ubs;
    for (node u : g.nodes()) {
        if (local_ub_cont[u] > lower_bound) {
            nodes.push_back(u);
            in_nodes[u] = true;
            high_deg_ubs.push_back(local_ub_cont[u]);
        }
    }
    std::cerr <<"  local_ub_cont high deg only: ";
    distrib(high_deg_ubs).print(std::cerr);
    std::cerr << nodes.size() <<" nodes with degree >= 2^lb-1 = "
              << ((1 << lower_bound) - 1) <<"\n";

    if (nodes.size() == 0 || lower_bound >= upper_bound) return lower_bound;

        
    // ----------------- Choose ordering for high deg. nodes --------------

    std::vector<uint> kcore_rank;

    if (ord == ord_kcore) {
    
        std::vector<uint> prerank(g.nb_nodes());
        for (node u : g.nodes()) {
            if (local_ub_cont[u] > lower_bound) {
                prerank[u] = 1;
            } else {
                prerank[u] = 0;
            }
        }
    
        kcore_rank = g.kcore_ranking(prerank);
    
        std::sort(nodes.begin(), nodes.end(),
                  [&kcore_rank](const node u, const node v) {
                      return kcore_rank[u] < kcore_rank[v];
                  });

    } else if (ord == ord_degree) {

        std::sort(nodes.begin(), nodes.end(),
                  [this](const node u, const node v) {
                      return g.degree(u) < g.degree(v);
                  });

    } else if (ord == ord_degree_dec) {

        std::sort(nodes.begin(), nodes.end(),
                  [this](const node u, const node v) {
                      return g.degree(u) > g.degree(v);
                  });

    } else if (ord == ord_locub) {

        compute_local_upper_bounds();
        std::sort(nodes.begin(), nodes.end(),
                  [this](const node u, const node v) {
                      return local_ub[u] < local_ub[v];
                  });
        
    } else if (ord == ord_random) {

        //std::srand(1234); // for reprocudibility
        std::srand(std::time(nullptr)); // for variability
        for (uint i = nodes.size(); i > 1; --i) {
            uint j = std::rand() % i;
            std::swap(nodes[i-1], nodes[j]);
        }
        
    } else {
        assert(false); // Unknown ordering ord!
    }

    
    // ------------ rank high deg nodes according to ordering ---------------
    
    /*
    for (uint i = 0, j = nodes.size() - 1 - i; i != 9; i+=3, j-=1) {
        std::swap(nodes[i], nodes[j]);
    }
    */
    
    for (uint i = 0; i < nodes.size(); ++i) { rank[nodes[i]] = i;}

    
    // -------------- better ub_cont through kcore elim + bound -------------

    std::vector<uint> & kcore_ub_cont = local_ub_cont;
        
    if (kcore_ubs) {

        if (kcore_rank.size() == 0) {
            kcore_rank = g.kcore_ranking();
        }
    
        std::vector<uint> rank_all(g.nb_nodes());
        // high deg nodes after:
        for (node u : g.nodes()) {
            if (in_nodes[u]) rank_all[u] = g.nb_nodes() + rank[u];
            else rank_all[u] = kcore_rank[u];
        }
        kcore_ub_cont = kcore_elim_ub(rank_all);
        
    } 

    assert(kcore_ub_cont.size() >= g.nb_nodes());
        
    
    // --- Use local upper bounds (on shat. set included in N[u] for all u) ---

    uint nb_loc_max = 0;
        
    if (local_ubs) {
        compute_local_upper_bounds(1);
        nb_loc_max = update_upper_bound();
    }


    // -- Sparse version of main loop: to obtain tight local ub on many nodes --

    if (sparse) {
        sparse_ub_cont(std::max(lower_bound - 1,uint(3)), ball2);
    }

    // -- Just measure overhead. --

    if (just_overhead) {
        return 0;
    }

    
    // ---- Main loop: scan all shattered subsets of high deg nodes --------

    std::vector<uint> ball_seen(g.nb_nodes(), false);

    nb_shattered = 0;
    nb_visits_total = 0;
    double bsize_sum = 0.;
    double deg_sum = 0.;
    uint root_nb = 0;
    
     for (uint xi = 0; xi < nodes.size(); ++xi) { 
        node x = nodes[xi]; // Search shattered sets S containing x and nodes after x:
        assert(rank[x] == xi);
        progress(xi, nodes.size(),1);

        local_ub_cont[x] = std::min(local_ub_cont[x],
                                    std::max(lower_bound, kcore_ub_cont[x]));
        
        if (local_ub_cont[x] > lower_bound) {

            root_nb += 1;

            if (ball2) {
                // Compute B(x,2) \cap { remaining high deg nodes }:
                x_ball.clear();
                for (node v : g.neighbors(x)) {
                    if (g.degree(v) + 1 <= lower_bound) continue; // if a shat. set of size > lower_bound contains x and y, some node v must be neighbor of all shat. set and thus has deg+1 > lower_bound
                    deg_sum += 1.;
                    for (node y : g.neighbors(v)) {
                        if ( ( ! ball_seen[y] ) // already added
                             && rank[y] >= xi // y comes after x in the ordering
                             && local_ub_cont[y] > lower_bound // we look for a shatttered set greater than lower_bound
                             ) {
                            x_ball.push_back(y);
                            ball_seen[y] = true;
                        }
                    }
                }
                for (node y : x_ball) { ball_seen[y] = false; }
                std::sort(x_ball.begin(), x_ball.end(),
                          [this](const node u, const node v) {
                              return rank[u] < rank[v];
                          });
                assert(x_ball.front() == x);

                bsize_sum += double(x_ball.size());

                compute_shattered_with(local_ub_cont[x],
                                       x_ball.begin(), x_ball.end(),
                                       empty_shat_set, 0);

            } else {

                compute_shattered_with(local_ub_cont[x],
                                       nodes.begin() + xi, nodes.end(),
                                       empty_shat_set, 0);

            }
        }
            
        if (lower_bound >= upper_bound) break;

        if (local_ubs) { // update local upper bounds when excluding x:
            for(node y : g.neighbors(x)) {
                local_ub_inter[y].pop_back();
                uint ub = local_ub_inter[y].back();
                if (ub < local_ub[y]) {
                    if (local_ub[y] >= upper_bound) {
                        assert(nb_loc_max > 0);
                        --nb_loc_max;
                    }
                    local_ub[y] = ub;
                }
            }
            if (nb_loc_max == 0) {
                nb_loc_max = update_upper_bound();
                if (lower_bound >= upper_bound) break;
            }
        }
        
    }

     std::cerr <<"nb_shattered="<< nb_shattered <<"\n";
     std::cerr <<"nb_visits_total="<< nb_visits_total <<"\n";
     std::cerr <<"nb_root_balls="<< root_nb <<"\n";
     std::cerr <<"avg_degree="<< deg_sum / double(root_nb) <<"\n";
     std::cerr <<"avg_ball_size="<< bsize_sum / double(root_nb) <<"\n";

    return lower_bound;
}


uint vcdim::compute_shattered_with
    (uint loc_ub,
     std::vector<node>::iterator x_ball_it, // cur. node in array of candidates
     std::vector<node>::iterator x_ball_end, // end of array
     const shat_set_table & sh_par, // table corresponding to current shat set
     uint size // current shat set size
     ) {
    node x = *(x_ball_it);
    ++nb_visits_total;
    loc_ub = std::min(loc_ub, local_ub_cont[x]);
    if (loc_ub <= lower_bound) return lower_bound;
    if (g.degree(x) + 1 < sh_par.table.size()) return lower_bound; // not shattered : not enough traces to fill the second half of the table
    shat_set_table sh(sh_par);
    std::vector<uint> & table = sh.table;
    assert(size + 1 < 8*sizeof(mask)); // mask overflow, vcdim is too high
    mask mask_x = mask(1) << size;
    uint loc_ub_max = 0;
    bool continue_growing = true;
    uint min_size_part = 1 << (lower_bound + 1 - size - 1); // minimum size of parts to allow to grow to a shattered set of size lower_bound+1 or more
    for (node u : g.neighbors(x)) {
        mask y_u = node_mask[u]; // trace of u on parent shat set (without x)
        assert(table[y_u] > 0);
        table[y_u] -= 1; // was counted before adding x
        if (table[y_u] < min_size_part) {
            return lower_bound; // cannot grow > lower_bound
        } else if (table[y_u] <= 1) {
            continue_growing = false; // cannot split into two classes
        }
        y_u ^= mask_x;      // trace with x
        table[y_u] += 1; // one more neighbor with trace y_u
        if (y_u == (mask_x << 1) - 1) { // u is neighbor of all shat_set
            loc_ub_max = std::max(loc_ub_max, local_ub[u]);
        }
    }
    loc_ub = std::min(loc_ub, loc_ub_max);
    for (mask y = mask_x; y < (mask_x << 1); ++y) {
        if (table[y] < min_size_part) {
            return lower_bound; // cannot grow > lower_bound
        } else if (table[y] <= 1) {
            continue_growing = false; // cannot split into two classes
        }
    }
    shat_set.push_back(x);
    ++size;
    ++nb_shattered;
    uint max_shat = size;
    if (update_lower_bound(max_shat)) {
        std::cerr <<"  loc_ub="<< loc_ub
                  <<" : "<< shat_set <<" is shattered\n";
    }
    if (continue_growing) {
        for (node y : g.neighbors(x)) node_mask[y] ^= mask_x; // update traces
        while (++x_ball_it != x_ball_end && max_shat < loc_ub
                 && loc_ub > lower_bound && upper_bound > lower_bound) {
            uint mshat = compute_shattered_with(loc_ub,
                                               x_ball_it, x_ball_end, sh, size);
            if (mshat > max_shat) { max_shat = mshat; }
        }
        for (node y : g.neighbors(x)) node_mask[y] ^= mask_x; // restore traces
    }
    shat_set.pop_back();
    return max_shat;
}



uint64_t vcdim::compute_nb_shattered(uint lb) {

    // Compute degrees and associated local upper bounds.
    
    init_ub();
    
    // --------- Only high deg nodes can be part of a large shattered set ---

    lower_bound = lb;
    
    nodes.clear();
    for (node u : g.nodes()) {
        if (local_ub_cont[u] > lb) {
            nodes.push_back(u);
            in_nodes[u] = true;
        }
    }
    std::cerr << nodes.size() <<" nodes with degree >= 2^lb-1 = "
              << ((1 << lb) - 1) <<"\n";

    // node ordering does not matter here

    for (uint i = 0; i < nodes.size(); ++i) { rank[nodes[i]] = i;}

    // ---- Main loop: scan all shattered subsets of high deg nodes --------

    std::vector<uint> ball_seen(g.nb_nodes(), false);

    nb_shattered = 0;
    nb_visits_total = 0;
    
    for (uint xi = 0; xi < nodes.size(); ++xi) { 
        node x = nodes[xi]; // Search shattered sets S containing x and nodes after x:
        assert(rank[x] == xi);
        progress(xi, nodes.size(),1);

        x_ball.clear();
        for (node v : g.neighbors(x)) {
            for (node y : g.neighbors(v)) {
                if ( ( ! ball_seen[y] ) // already added
                     && rank[y] >= xi // y comes after x in the ordering
                     && local_ub_cont[y] > lb // high degree node
                     ) {
                    x_ball.push_back(y);
                    ball_seen[y] = true;
                }
            }
        }
        for (node y : x_ball) { ball_seen[y] = false; }
        std::sort(x_ball.begin(), x_ball.end(),
                  [this](const node u, const node v) {
                      return rank[u] < rank[v];
                  });
        assert(x_ball.front() == x);

        compute_shattered_all(x_ball.begin(), x_ball.end(),
                              empty_shat_set, 0);

    }
    
    std::cerr <<"nb_shattered="<< nb_shattered <<"\n";
    std::cerr <<"nb_visits_total="<< nb_visits_total <<"\n";

    return lower_bound;

}

uint vcdim::compute_shattered_all
    (std::vector<node>::iterator x_ball_it, // cur. node in array of candidates
     std::vector<node>::iterator x_ball_end, // end of array
     const shat_set_table & sh_par, // table corresponding to current shat set
     uint size // current shat set size
     ) {
    node x = *(x_ball_it);
    ++nb_visits_total;
    if (g.degree(x) + 1 < sh_par.table.size()) return lower_bound; // not shattered : not enough traces to fill the second half of the table
    shat_set_table sh(sh_par);
    std::vector<uint> & table = sh.table;
    assert(size + 1 < 8*sizeof(mask)); // mask overflow, vcdim is too high
    mask mask_x = mask(1) << size;
    bool continue_growing = true;
    uint min_size_part = 1; // minimum size of parts of a shattered set
    for (node u : g.neighbors(x)) {
        mask y_u = node_mask[u]; // trace of u on parent shat set (without x)
        assert(table[y_u] > 0);
        table[y_u] -= 1; // was counted before adding x
        if (table[y_u] < min_size_part) {
            return lower_bound; // not shattered
        } else if (table[y_u] <= 1) {
            continue_growing = false; // cannot split into two classes
        }
        y_u ^= mask_x;      // trace with x
        table[y_u] += 1; // one more neighbor with trace y_u
    }
    for (mask y = mask_x; y < (mask_x << 1); ++y) {
        if (table[y] < min_size_part) {
            return lower_bound; // not shattered
        } else if (table[y] <= 1) {
            continue_growing = false; // cannot split into two classes
        }
    }
    shat_set.push_back(x);
    ++size;
    ++nb_shattered;
    uint max_shat = size;
    if (update_lower_bound(max_shat)) {
        std::cerr << shat_set <<" is shattered\n";
    }
    if (continue_growing) {
        for (node y : g.neighbors(x)) node_mask[y] ^= mask_x; // update traces
        while (++x_ball_it != x_ball_end) {
            uint mshat = compute_shattered_all(x_ball_it, x_ball_end, sh, size);
            if (mshat > max_shat) { max_shat = mshat; }
        }
        for (node y : g.neighbors(x)) node_mask[y] ^= mask_x; // restore traces
    }
    shat_set.pop_back();
    return max_shat;
}


uint vcdim::compute_vcdim_loc() {

    init_bounds();
    greedy_vcdim_lb();
    
    if (nodes.size() == 0) return lower_bound;

    std::vector<uint> kcore_rank = g.kcore_ranking();
    
    // Ordering for processing nodes: high deg first to have local upper bounds decrease faster.
    std::sort(nodes.begin(), nodes.end(),
              [this](const node u, const node v) {
                  // BOF: return kcore_rank[u] < kcore_rank[v];
                  //return ball_size[u] < ball_size[v]; //BS+ DIMES: 8m36
                  /*
                  return log_2(ball_size[u]) < log_2(ball_size[v])//lBS+,UB+ : 7m53
                             || (log_2(ball_size[u]) == log_2(ball_size[v])
                                 && local_ub[u] < local_ub[v]
                                 );
                  */
                  return local_ub[u] < local_ub[v] 
                         || (local_ub[u] == local_ub[v]
                             && (local_ub_cont[u] < local_ub_cont[v]
                                 || (local_ub_cont[u] == local_ub_cont[v]
                                     && local_lb[u] < local_lb[v]))
                             );
                  //lDEG-,BS+ : 4m1 // better local_ub in the end but slower
                  /*
                  return local_ub_cont[u] < local_ub_cont[v] //lDEG+,BS+ : 4m1
                         || (local_ub_cont[u] == local_ub_cont[v]
                             && ball_size[u] < ball_size[v]
                             );
                  */
                  
              });

    // helps for finding higher lb quickly: (helps for lBS+,UB+ and BS+)
    /*
    for (uint i = 0, j = nodes.size() - 1 - i; i != 9; i+=3, j-=1) {
        std::swap(nodes[i], nodes[j]);
    }
    */
    
    /* Random: // DIMES: 4m39
    for (uint i = nodes.size(); i > 1; --i) {
        uint j = std::rand() % i;
        std::swap(nodes[i-1], nodes[j]);
    }
    */

    // std::vector<uint> kcore_ub_cont = kcore_elim_ub(g.kcore_ranking()); // good bounds but bad ordering

    // sort_nodes_by_ub_score(); // does not seem a good idea

    //sort_kcore_ub();
    
    for (uint i = 0; i < nodes.size(); ++i) { rank[nodes[i]] = i;}

    //std::vector<uint> kcore_rank = g.kcore_ranking();
    std::vector<uint> rank_all(g.nb_nodes());
    // high deg nodes after:
    for (node u : g.nodes()) {
        if (in_nodes[u]) rank_all[u] = g.nb_nodes() + rank[u];
        else rank_all[u] = kcore_rank[u];
    }
    std::vector<uint> kcore_ub_cont = kcore_elim_ub(rank_all);
    
    // Update local bounds restricted to high deg nodes (those in [nodes]):
    compute_local_upper_bounds(upper_bound - lower_bound);
    uint nb_loc_max = update_upper_bound();
    std::cerr <<" deg with high deg nodes: ";
    distrib(g.nb_nodes(),
            [this](size_t i) { return local_ub_inter[i].size(); }).print(std::cerr);
    std::cerr <<" local_ub2: ";
    distrib(g.nb_nodes(),
            [this](size_t i) { return local_ub[i]; }).print(std::cerr);
    std::cerr <<" highdegnodes: ";
    distrib(nodes.size(),
            [this](size_t i) { return g.degree(nodes[i]); }).print(std::cerr);
    std::cerr <<" highdegnodes with high deg: ";
    distrib(nodes.size(),
            [this](size_t i) { return local_ub_inter[nodes[i]].size(); }).print(std::cerr);
    std::cerr <<" ballsize: ";
    distrib(nodes.size(),
            [this](size_t i) { return ball_size[nodes[i]]; }).print(std::cerr);
    std::cerr <<" local_lb2highdeg: ";
    distrib(nodes.size(),
            [this](size_t i) { return local_lb[nodes[i]]; }).print(std::cerr);
    std::cerr <<" local_ub2highdeg: ";
    distrib(nodes.size(),
            [this](size_t i) { return local_ub[nodes[i]]; }).print(std::cerr);
     if (lower_bound >= upper_bound) return lower_bound;

    // Scan all shattered subsets of high deg nodes:
     for (uint xi = 0; xi < nodes.size(); ++xi) { 
        node x = nodes[xi]; // Search shattered sets S containing x:
        assert(rank[x] == xi);
        progress(xi, nodes.size(),1);

        local_ub_cont[x] = std::min(local_ub_cont[x], std::max(lower_bound, kcore_ub_cont[x]));
        
        if (local_ub_cont[x] > lower_bound) {
            // Compute B(x,2):
            part.init();
            for (node v : g.neighbors(x)) {
                part.split_left(g.neighbors(v));
            }
            x_ball.clear();
            for (node y : part) {
                if (rank[y] >= xi && local_ub_cont[y] > lower_bound) {
                    x_ball.push_back(y);
                }
            }
            std::sort(x_ball.begin(), x_ball.end(),
                      [this,x](const node u, const node v) {
                          return u == x
                              || (v != x && rank[u] < rank[v]);
                      });
            assert(x_ball.front() == x);

            compute_shattered_with(local_ub_cont[x],
                                   x_ball.begin(),
                                   x_ball.end(),
                                   empty_shat_set, 0);
        }
            
        if (lower_bound >= upper_bound) break;
        
        // update local upper bounds when excluding x:
        for(node y : g.neighbors(x)) {
            local_ub_inter[y].pop_back();
            uint ub = local_ub_inter[y].back();
            if (ub < local_ub[y]) {
                if (local_ub[y] >= upper_bound) {
                    assert(nb_loc_max > 0);
                    --nb_loc_max;
                }
                local_ub[y] = ub;
            }
        }
        if (nb_loc_max == 0) {
            nb_loc_max = update_upper_bound();
            if (lower_bound >= upper_bound) break;
        }
        
    }

    return lower_bound;
}

 

void vcdim::compute_local_upper_bounds(uint nb_opt_max) {
    local_ub_inter.clear(g.nb_nodes());

    for (node u : g.nodes()) {

        /*
        if (g.degree(u) <= lower_bound && local_ub_cont[u] <= lower_bound) {
            local_ub[u] = lower_bound;
            continue;
        }
        */
        

        // Sort neighborhood of u:
        x_ball.clear();
        for (node v : g.neighbors(u)) {
            if (in_nodes[v]) { x_ball.push_back(v); }
        }
        std::sort(x_ball.begin(), x_ball.end(),
                  [this](const node u, const node v) {
                      return rank[u] > rank[v]; // small rank last
              });

        auto [lb, ub, bs] = local_bounds(x_ball, local_ub_inter[u],
                                         nb_opt_max);
        local_lb[u] = std::max(local_lb[u], lb);
        update_lower_bound(lb);
        if (ub < local_ub[u]) local_ub[u] = std::max(ub, lower_bound);
        ball_size[u] = bs;
        
        if (lower_bound >= upper_bound) break;
    }
}

std::tuple<uint, uint, uint> vcdim::local_bounds(const std::vector<node> & set,
                                                 std::vector<uint> & partial_ub,
                                                 uint nb_opt_max)
{
    partial_ub.clear();
    partial_ub.reserve(set.size() + 1);
    // partial_ub[i] will contain an upper bound on the size of a shattered subset included in the first i nodes of set.
    partial_ub.push_back(0); // ub=0 when no node is included

    // Max degree among i neighbors with highest degree:
    uint ub_max = 0, lb_max = 0;
    deg_ub_count.clear();
    part.init();
    uint n_split = 0;
    for (node x : set) {
        uint ub = local_ub_cont[x]; // ub of any shattered set containing x
        ub_max = std::max(ub, ub_max);
        
        // max size ubeff of a subset where all nodes have ub >= ubeff:
        ++(deg_ub_count[ub]);
        uint ubeff = ub_max, count = deg_ub_count[ub_max];
        while (count < ubeff) {
            --ubeff;
            count += deg_ub_count[ubeff];
        }

        if (ub > lower_bound) {// consider only shattered sets of size > lower_bound, optim bellow with large nb_opt_max does better
            part.split_left(g.neighbors(x));
            ++n_split;
            shat_set.push_back(x); // we find a shat set if n_split==lb_max
            lb_max = std::max(lb_max, part.dual_vcdim_lower_bound(n_split));
            if (update_lower_bound(lb_max) && n_split == lb_max) {
                std::cerr <<"  "<< shat_set <<" is shattered\n";
            }
        }
        uint ubparts = log_2(part.nb_parts()); // if there exists a shattered subset of size j, there must be at least 2^j parts
        
        if (ubparts < lower_bound) ubparts = lower_bound; // because of above

        partial_ub.push_back(std::min(ubeff, ubparts)); 
    }
    shat_set.clear();

    uint ball_size = part.count_split_elements(); // size of N[set]
    /* for (node y : part) {
         if (local_ub_cont[y] > lower_bound)
            ++ball_size; // count only high deg nodes
       }
    */
    
    uint ub_all = partial_ub[set.size()];

    // Try to improve further ub_all and intermediate bounds:
    uint nb_opt = 0, ub_opt = ub_all, highest_to_improve = ub_all;
    while(nb_opt < nb_opt_max && highest_to_improve > lower_bound) {
        ++nb_opt;
        highest_to_improve = 0;
        part.init();
        uint ubparts = 0, n_split=0;
        uint xi = 0;
        for (node x : set) {
            ++xi;
            uint ub = local_ub_cont[x];
            if (ub >= ub_opt) {
                part.split_left(g.neighbors(x));
                ++n_split;
                lb_max = std::max(lb_max, part.dual_vcdim_lower_bound(n_split));
            }
            ubparts = log_2(part.nb_parts());
            if (partial_ub[xi] >= ub_opt && ubparts < partial_ub[xi]) {
                partial_ub[xi] = std::max(ub_opt - 1, ubparts);
            }
            if (partial_ub[xi] <= ub_opt - 1) {
                highest_to_improve = partial_ub[xi]; // partial_ub is in non-decreasing order
            }
        }
        ub_opt = highest_to_improve;
    }
    //if (nb_opt > 1) { std::cerr << nb_opt; }

    ub_all = partial_ub[set.size()];
    
    return { lb_max, ub_all, ball_size };
}


bool vcdim::update_lower_bound(uint lb) {
    if (lb > lower_bound) {
        lower_bound = lb ;
        std::cerr <<"lb="<< lower_bound<<" ub="<< upper_bound <<"\n";
        return true;
    }
    return false;
}


uint vcdim::update_upper_bound() {
    uint nb = 0, loc_ub_max = 0;
    for (node u : g.nodes()) {
        if (local_ub[u] >= loc_ub_max) {
            if (local_ub[u] == loc_ub_max) {
                ++nb;
            } else {
                loc_ub_max = local_ub[u];
                nb = 1;
            }
        }
    }
    assert(loc_ub_max <= upper_bound);
    if (loc_ub_max < upper_bound) {
        upper_bound = loc_ub_max;
        std::cerr <<"lb="<< lower_bound <<" ub="<< upper_bound
                  <<std::endl;
    }
    return nb;
}





void vcdim::sort_nodes_by_ub_score() {

    std::vector<uint> ub_lb(g.nb_nodes(), 0), score(g.nb_nodes(), 0);
    std::vector<bool> was_one(g.nb_nodes(), false);
    
    for (node u : g.nodes()) {
        if (local_ub[u] > lower_bound) {
            ub_lb[u] = local_ub[u] - lower_bound;
            if (ub_lb[u] == 1) { // target mainly those easier to bring to lb
                was_one[u] = true;
                for (node v : g.neighbors(u)) {
                    if (local_ub_cont[v] > lower_bound) { // high deg node
                        score[v] += 1;
                    }
                }
            }
        }
    }

    using val = std::pair<node, uint>;
    using cont = std::vector<val>;
    auto cmp = [this](const val & us, const val & vs) {
        node u = us.first, v = vs.first;
        return us.second < vs.second
            || (us.second == vs.second
                && (local_ub_cont[u] > local_ub_cont[v]
                         || (local_ub_cont[u] == local_ub_cont[v]
                             && ball_size[u] > ball_size[v]
                             )
                    ));
        };
    std::priority_queue<val, cont, decltype(cmp)> queue(cmp);

    for (node u : nodes) { queue.push(val{u, score[u]}); }

    assert(nodes.size() == queue.size());
    for (node u : nodes) { in_nodes[u] = false; }
    nodes.clear();
    for (std::cerr<<"scores :"; ! queue.empty(); queue.pop()) {
        const val & xs = queue.top();
        node x = xs.first;
        if (score[x] != xs.second || in_nodes[x]) continue;
        nodes.push_back(x);
        in_nodes[x] = true;
        uint real_score = 0;
        for (node v : g.neighbors(x)) {
            if (ub_lb[v] > 0) {
                // recompute v's upper bound
                x_ball.clear();
                for (node y : g.neighbors(v)) {
                    if (local_ub_cont[y] > lower_bound && ! in_nodes[y]) {
                        x_ball.push_back(y);
                    }
                }
                auto [_, ub, __] = local_bounds(x_ball, local_ub_inter[v],
                                                 upper_bound - lower_bound);
                ub_lb[v] = ub - lower_bound;
                if (ub_lb[v] == 0 && was_one[v]) { // no more score
                    ++real_score;
                    for (node y : g.neighbors(v)) {
                        if (local_ub_cont[y] > lower_bound // high deg node
                            && ! in_nodes[y]) {
                            score[y] -= 1;
                            queue.push(val{y, score[y]});
                        }
                    }
                } else if (false && ub_lb[v] == 1 && ! was_one[v]) { // re-score ?
                    for (node y : g.neighbors(v)) {
                        if (local_ub_cont[y] > lower_bound // high deg node
                            && ! in_nodes[y]) {
                            score[y] += 1;
                            queue.push(val{y, score[y]});
                        }
                    }
                }
            }
        }
        std::cerr << x <<","<< score[x] <<","<< real_score
                  <<","<< ball_size[x] <<" ";
    }
    std::cerr <<"\n"<< nodes.size() <<" high deg nodes\n";

}


std::vector<uint> vcdim::sort_kcore_ub() {

    // Compute coreness of each node according to a kcore ordering with [nodes] at the end
    std::vector<uint> rank = g.kcore_ranking();
    for (node u : g.nodes()) {
        if (in_nodes[u]) rank[u] = 2 * g.nb_nodes(); // at the end
    }

    // bring some high ub nodes in front
    for (uint i=0; i < 3; ++i) {
        rank[nodes[i]] = g.nb_nodes() - i - 1;
    }

    std::vector<uint> higher_deg(g.nb_nodes());
    uint kmax = 0;
    for (node u : g.nodes()) {
        uint ku = 0;
        for (node v : g.neighbors(u)) {
            if (rank[v] >= rank[u]) { ++ku; }
        }
        higher_deg[u] = ku;
        kmax = std::max(kmax, ku);
    }
    std::cerr <<"Degrmax: "<< kmax <<"\n";
    std::cerr <<"higher-rank degree distr: ";
    distrib(g.nb_nodes(), [&higher_deg](size_t i) {return higher_deg[i];}).print(std::cerr);

    // Compute upper bounds after eliminating low doeg nodes
    std::vector<uint> ub_cont(g.nb_nodes()); // ub that we get
    std::vector<uint> nb_k_distr(kmax+1,0);
    
    auto get_ub = [this, kmax, &rank, &higher_deg, &nb_k_distr](node u) {
        uint deg = 0, deg_right = 0, deg_left = 0;
        for (uint & nb_k : nb_k_distr) { nb_k = 0; }
        for (node v : g.neighbors(u)) {
            if (rank[v] < rank[u]) {
                nb_k_distr[higher_deg[v]] += 1;
                ++deg_left; // does not count u itself
            } else { ++deg_right; } // counts u itself
            ++deg; // counts u itself
        }
        assert(deg == g.degree(u) + 1);
        assert(deg_left + deg_right == deg);
        uint ub = 2; // upper bound on shat set.
        while (true) { // count traces with u
            uint sum = 0;
            uint deg_excess = 0, prefix_sum = 0; // check count
            uint binom = 1; // (ub-1 choose 0)
            uint i = 1, rem_left = 0, rem_binom = 0;
            for ( ; i < ub; ++i) {
                if (i > 1) binom = binom * (ub - i + 1) / (i-1);//(ub-1 choose i-1)
                rem_left += nb_k_distr[i];
                rem_binom += binom;
                prefix_sum += nb_k_distr[i];
                if (rem_left > rem_binom) {
                    // all neighbors counted in rem_left have trace of size at most i, and cannot produce more traces than those counted in rem_binom
                    deg_excess += rem_left - rem_binom;
                    sum += rem_binom;
                    rem_left = 0;
                    rem_binom = 0;
                }
            }
            // count remaining left neighbors:
            sum += rem_left;
            for ( ; i <= kmax; ++i) {
                sum += nb_k_distr[i];
                prefix_sum += nb_k_distr[i];
            }
            // and right neighbors:
            sum += deg_right; 
            assert(sum + deg_excess == deg); // check our neighbor count
            if (sum < uint(1) << (ub - 1)) { --ub; break; }
            ++ub;
        }
        return ub;
    };

    uint ubmax = 0;
    for (node u : g.nodes()) {
        ub_cont[u] = in_nodes[u] ? get_ub(u) : local_ub_cont[u];
        ubmax = std::max(ubmax, ub_cont[u]);
    }

    // Priority queue
    std::vector<std::vector<node>> nodes_by_ub(ubmax + 1);
    std::vector<uint> index(g.nb_nodes());
    for (node u : nodes) {
        uint ub = ub_cont[u];
        index[u] = nodes_by_ub[ub].size();
        nodes_by_ub[ub].push_back(u);
    }

    // rank high deg nodes by small ub first
    uint ub = 0, r = g.nb_nodes();
    for (uint i = 0; i < nodes.size(); ) {
        while (ub <= ubmax && nodes_by_ub[ub].size() == 0) ++ub;
        assert(ub <= ubmax);
        while (nodes_by_ub[ub].size() > 0) {
            node u = nodes_by_ub[ub].back();
            rank[u] = r++;
            ++i;
            nodes_by_ub[ub].pop_back();
            uint ubmin = ub;
            for (node v : g.neighbors(u)) {
                if (rank[v] > rank[u]) {
                    assert(in_nodes[v]);
                    uint ub_v = ub_cont[v];
                    uint i = index[v];
                    node w = nodes_by_ub[ub_v].back();
                    std::swap(nodes_by_ub[ub_v][i], nodes_by_ub[ub_v].back());
                    index[w] = i;
                    nodes_by_ub[ub_v].pop_back(); // pop v
                    ub_v = get_ub(v);
                    ubmin = std::min(ubmin, ub_v);
                    ub_cont[v] = ub_v;
                    index[v] = nodes_by_ub[ub_v].size();
                    nodes_by_ub[ub_v].push_back(v);
                }
            }
            if (ubmin < ub) {
                ub = ubmin;
                break;
            }
        }
    }
    for (node u : nodes) { assert(rank[u] < 2*g.nb_nodes()); } // check all done

    std::cerr <<"vcdim_ub_cont distr: ";
    distrib(g.nb_nodes(), [&ub_cont](size_t i) {return ub_cont[i];}).print(std::cerr);
    
    // Sort high degree nodes:
    std::sort(nodes.begin(), nodes.end(),
              [&rank](const node u, const node v) {
                  return rank[u] < rank[v];
              });
    std::cerr <<"vcdim_ub_cont distr high deg: ";
    distrib(nodes.size(), [&ub_cont,this](size_t i) {return ub_cont[nodes[i]];}).print(std::cerr);

    return ub_cont;
}


std::vector<uint> vcdim::kcore_elim_ub(const std::vector<uint> & rank) {
    // Compute coreness of each node according to rank order:
    std::vector<uint> higher_deg(g.nb_nodes());
    uint kmax = 0;
    for (node u : g.nodes()) {
        uint ku = 0;
        for (node v : g.neighbors(u)) {
            if (rank[v] > rank[u]) { ++ku; }
        }
        higher_deg[u] = ku;
        kmax = std::max(kmax, ku);
    }
    std::cerr <<"Degeneracy of given ranking: "<< kmax <<"\n";
    std::cerr <<"higher-rank degree distr: ";
    distrib(g.nb_nodes(), [&higher_deg](size_t i) {return higher_deg[i];}).print(std::cerr);
    
    // Compute VC-dim bounds we can obtain :
    std::vector<uint> ub_cont(g.nb_nodes()), nb_k_distr(kmax+1,0);
    for (node u : g.nodes()) {
        uint deg = 0, deg_right = 0, deg_left = 0;
        for (uint & nb_k : nb_k_distr) { nb_k = 0; }
        for (node v : g.neighbors(u)) {
            if (rank[v] < rank[u]) {
                nb_k_distr[higher_deg[v]] += 1;
                ++deg_left; // does not count u itself
            } else { ++deg_right; } // counts u itself
            ++deg; // counts u itself
        }
        assert(deg == g.degree(u) + 1);
        assert(deg_left + deg_right == deg);
        uint ub = 2; // upper bound on shat set containing u.
        while (true) { // count traces with u, assuming ub is tight
            uint sum = 0;
            uint deg_excess = 0, prefix_sum = 0; // check count
            uint64_t binom = 1; // (ub-1 choose 0)
            uint64_t uint_max =std::numeric_limits<uint>::max();
            uint i = 1, rem_left = 0, rem_binom = 0;
            for ( ; i < ub && i <= kmax; ++i) {
                if (i > 1) binom = binom * (ub - i + 1) / (i-1);//(ub-1 choose i-1)
                rem_left += nb_k_distr[i];
                assert(binom + rem_binom < uint_max);
                rem_binom += binom;
                prefix_sum += nb_k_distr[i];
                if (rem_left > rem_binom) {
                    // all neighbors counted in rem_left have trace of size at most i, and cannot produce more traces than those counted in rem_binom
                    deg_excess += rem_left - rem_binom;
                    sum += rem_binom;
                    rem_left = 0;
                    rem_binom = 0;
                }
            }
            // count remaining left neighbors:
            sum += rem_left;
            for ( ; i <= kmax; ++i) {
                sum += nb_k_distr[i];
                prefix_sum += nb_k_distr[i];
            }
            // and right neighbors:
            sum += deg_right;
            assert(prefix_sum == deg_left);
            assert(sum + deg_excess == deg); // check our neighbor count
            if (sum < uint(1) << (ub - 1)) { --ub; break; }
            ++ub;
        }
        ub_cont[u] = ub;
    }

    std::cerr <<"kcore_ub_cont distr: ";
    distrib(g.nb_nodes(), [&ub_cont](size_t i) {return ub_cont[i];}).print(std::cerr);
    
    std::cerr <<"kcore_ub_cont distr high deg: ";
    distrib(nodes.size(), [&ub_cont,this](size_t i) {return ub_cont[nodes[i]];}).print(std::cerr);

    return ub_cont;
}


void vcdim::reduce_graph(uint lb) {
    if (lb == 0) { lb = lower_bound + 1; }
    
    x_ball.clear();
    for (node u : g.nodes()) {
        if (local_ub_cont[u] >= lb) {
            x_ball.push_back(u);
        }
    }

    // keep also neighbors with specific traces:
    part.init();
    for (node u : x_ball) { part.split_left(g.neighbors(u)); }
    std::cerr << part.nb_parts() <<" parts vs "<< g.nb_nodes()
              <<" nodes, for "<< x_ball.size() <<" high deg nodes\n";
    for (uint i = 0; i < part.nb_elts() ; ) {
        // keep a representant of the part with min degree
        node u = part[i];
        uint deg_u = x_ball.size(); // some upper bound
        auto [ left, right ] = part.part_bounds(u);
        for (uint j=left; j < right; ++j) { // scan part
            node v = part[j];
            if (local_ub_cont[v] >= lb) {
                u = v; // ok already in x_ball
                break;
            }
            uint deg_v = 0;
            for (node w : g.neighbors(v)) {
                if (local_ub_cont[w] >= lb) {
                    ++deg_v;
                }
            }
            if (deg_v < deg_u) { u = v; }
        }
        if (local_ub_cont[u] < lb) {// not in x_ball
            x_ball.push_back(u); // need to add a representant
        }
        i = right; // first index in next part
    }
    std::cerr <<"subset size: "<< x_ball.size() <<"\n";

    std::sort(x_ball.begin(), x_ball.end()); // to allow update of mapping_to_orig bellow
    std::vector<node> mapping(g.nb_nodes(), node(-1));
    
    g = g.induced_subgraph(x_ball, mapping);

    for (uint i=0; i < x_ball.size(); ++i) {
        assert(i <= mapping[i]
               && (i == 0 || mapping[i-1] <= mapping[i]));
        mapping_to_orig[i] = mapping_to_orig[mapping[i]];
    }
    std::vector<node> twins = g.twin_classes();
    std::cerr << twins.size() <<" twin classes\n";

    init_ub();
    //greedy_vcdim_lb();
}

void progress(uint xi, size_t sz, uint incr) { // printerr progress [i/sz]
    for (uint p : { 1, 2, 5, 10, 25, 50, 75, 90, 95, 98, 99 }) {
        if (xi <= p * sz / 100 && xi + incr > p * sz / 100) {
            std::cerr <<" - "<< p <<"%\n";
        }
    }
}




void vcdim::sparse_ub_cont(uint incr, bool ball2) {
    std::vector<uint> ball_seen(g.nb_nodes(), false);

    std::cerr <<"sparse_ub_cont begin\n";

    for (uint xi = 0; xi < nodes.size(); xi += incr) { 
        node x = nodes[xi]; 
        progress(xi, nodes.size(), incr);

        if (local_ub_cont[x] > lower_bound) {

            uint ub = log_2(g.nb_nodes());

            if (ball2) {
                // Compute B(x,2) 
                x_ball.clear();
                for (node v : g.neighbors(x)) {
                    if (g.degree(v) + 1 <= lower_bound) continue; // if a shat. set of size > lower_bound contains x and y, some node v must be neighbor of all shat. set and thus has deg+1 > lower_bound
                    for (node y : g.neighbors(v)) {
                        if ( ( ! ball_seen[y] ) // already added
                             && in_nodes[y] // high deg. node
                             && local_ub_cont[y] > lower_bound // larger shat set
                             ) {
                            x_ball.push_back(y);
                            ball_seen[y] = true;
                        }
                    }
                }
                for (node y : x_ball) { ball_seen[y] = false; }
                std::sort(x_ball.begin(), x_ball.end(),
                          [this,x](const node u, const node v) {
                              return u == x || (v != x && rank[u] < rank[v]);
                          });
                assert(x_ball.front() == x);

                ub = compute_shattered_with(local_ub_cont[x], x_ball.begin(),
                                            x_ball.end(), empty_shat_set, 0);
            } else {
                ub = compute_shattered_with(local_ub_cont[x], nodes.begin()+xi,
                                            nodes.end(), empty_shat_set, 0);
            }
            
            local_ub_cont[x] = ub;
        }
    }

    std::cerr <<"sparse_ub_cont done\n";
}



bool exists_subset_shattered(const graph& g, partition& p,
                             const set_elts& neighb,
                             uint i, uint size, set_sets& s);
    

uint vcdim_lower_bound(const graph& g, const bool exact_vcdim) {
    uint lb = 0, ub = log_2(g.nb_nodes());
    partition p(g.nb_nodes());

    uint delta = 0;
    for (node u : g.nodes()) {
        if (g.degree(u) > delta) delta = g.degree(u);
    }
    if (1 + log_2(delta + 1) < ub) { ub = 1 + log_2(delta + 1); }
    std::cerr <<" ub="<< ub <<std::endl;
    
    uint loc_ub_max = 0;
    
    for (node u : g.nodes()) {

        if (g.degree(u) <= lb) continue;

        uint l = p.dual_vcdim_lower_bound(g, g.neighbors(u));

        uint d = 0;
        for (node v : g.neighbors(u)) {
            if (g.degree(v) > d) d = g.degree(v);
        }

        uint loc_ub = std::min(log_2(p.nb_parts()), 1+log_2(d+1));
        if (loc_ub > loc_ub_max) {
            loc_ub_max = loc_ub;
            std::cerr <<" ub="<< ub
                      <<" loc_ub_max="<< loc_ub_max <<std::endl;
        }
        
        if (exact_vcdim && l < loc_ub) {
            set_sets s;
            for (uint sh = l+1; sh <= loc_ub; ++sh) {
                s.clear();
                if (exists_subset_shattered(g, p, g.neighbors(u),
                                            0, sh, s)) {
                    l = sh;
                    if (l > lb) {
                        std::cerr <<"  "<< s <<" is shattered\n";
                    }
                } else {
                    break;
                }
            }
        }
        
        if (l > lb) {
            lb = l ;
            std::cerr <<"lb="<< lb <<" ub="<< ub
                      <<" (at u="<< u
                      <<" deg "<< g.degree(u)
                      <<" d="<< d <<" loc_ub="<< loc_ub
                      <<" loc_ub_max="<< loc_ub_max
                      <<")\n";
        }
        
        if (lb >= ub) break;

    }
    std::cerr <<"lb="<< lb <<" ub="<< ub
              <<" loc_ub_max="<< loc_ub_max <<std::endl;
    return lb;
}

bool exists_subset_shattered(const graph& g, partition& p,
                             const set_elts& neighb,
                             uint i, uint size, set_sets& s) {
    if (s.size() == size) {
        return p.dual_shattered(g, s);
    } else {
        for (uint j=i; j < neighb.size(); ++j) {
            s.push_back(neighb[j]);
            if (exists_subset_shattered(g, p, neighb, j+1, size, s)) {
                return true;
            }
            s.pop_back();
        }
        return false;
    }
}



void vcdim_unit() {
    std::cout <<"------------------- vcdim::unit --------------------\n";

    graph g;
    std::vector<std::vector<int> > edges = {
        {0, 1}, {0, 2}, {1, 2}, {0, 3}, {1, 4}, {2, 5}, {3,6},
        {7, 0}, {7, 1}, {8, 1}, {8, 2}, {9, 2}, {9, 0}, {10, 6}
    }; // {0, 1, 2} is shattered
    for (auto pair : edges) { g.add_edge(pair[0], pair[1]); }

    vcdim vcg(g);
    std::cout <<"vcdim g = "<< vcg.compute_vcdim() << std::endl;
    


    std::ifstream ifs("../unit_data/BIOGRID-SYSTEM-Dosage_Rescue-3.4.145.bcc.edgelist");
    graph bio(ifs);
    ifs.close();

    vcdim vc(bio);
    uint glb = vc.greedy_vcdim_lb(1);
    std::cout <<"greedy_lb = "<< glb <<"\n";

    uint comp_dim_bio = vc.compute_vcdim();
    std::cout <<"vcdim bio = "<< comp_dim_bio << std::endl;
    assert(comp_dim_bio == 4);
    
    partition p(bio.nb_nodes());
    set_sets s{1256, 952, 913, 709};
    bool shat = p.dual_shattered(bio, s);
    std::cout << s << (shat ? " is" : " is not") <<" shattered\n";
    assert(shat);

    std::cerr <<"N[709] = "<< bio.neighbors(709) <<std::endl;
    std::cerr <<"N[1256] = "<< bio.neighbors(1256) <<std::endl;
    set_sets s2{1256, 709};
    shat = p.dual_shattered(bio, s2);
    std::cout << s2 << (shat ? " is" : " is not") <<" shattered\n";
    assert(shat);
    
    s.push_back(794);
    shat = p.dual_shattered(bio, s);
    std::cout << s << (shat ? " is" : " is not") <<" shattered\n";
    assert( ! shat);
    
    set_sets s6{1416, 221, 709, 952, 702, 453};
    bool shat6 = p.dual_shattered(bio, s6);
    std::cout << s6 << (shat6 ? " is" : " is not") <<" shattered\n";
    
}
