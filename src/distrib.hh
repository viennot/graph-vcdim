/* Simple statistics on a set of integers.
 */

#pragma once

#include <functional>
#include <vector>
#include <fstream>

#include "integers.hh" // define uint

class distrib {

    std::vector<uint> elts;

public:

    distrib(std::vector<uint> e) : elts(e) {
        std::sort(elts.begin(), elts.end());
    }

    distrib(size_t size, std::function<uint(size_t)> make) ;

    void print(std::ostream & os, std::vector<uint> perct
               = { 1, 5, 10, 25, 50, 75, 90, 95, 99 }) ;

} ;
