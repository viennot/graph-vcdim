#include <cassert>
#include <iostream>
#include <cmath>
#include <algorithm>

#include "graph.hh"
#include "partition.hh"

partition::partition(const uint n)
    : elements(n), _n(n), slices(n), slice_of(n, 0), split_slices(n), pos_of(n),
      binomials(0)
{
    for (elt e = 0; e < _n; ++e) { elements[e] = e; }
    for (elt e = 0; e < _n; ++e) { pos_of[e] = e; }
    init();
    init_binomials();
}

void partition::init() {
    //uint count = full ? 0 : count_split_elements();
    slices.clear();
    slices.emplace_back(0, _n, not_a_slice);
    nb_slices = 1;
    split_slices.clear();
    for (elt e = 0; e < _n; ++e) { slice_of[e] = 0; }
    /* for (uint i = 0; i < count; ++i) {
         slice_of[elements[i]] = 0;
       }
    */
}

template<typename C>
void partition::insert(const C &container) {
    assert(false); // Obsolete
    assert(slices.size() == 1); // insert before any splitting
    for (typename C::const_iterator it = container.cbegin();
         it != container.cend(); ++it) {
        elt e = *it;
        assert(e >= 0 && e < _n);
        if (pos_of[e] == not_a_pos) {
            uint pos = elements.size();
            pos_of[e] = pos;
            elements.push_back(e);
            i_slice s = 0;
            slices[s].right++;
            slice_of[e] = s;
            //assert(check_element(e));
        }
    }
}

bool partition::check_element(uint e, bool verbose) {
    i_slice s = slice_of[e];
    slice slc = slices[s];
    uint pos = pos_of[e];
    bool ok = slc.left <= pos && pos < slc.right && e == elements[pos];
    if ( verbose || ! ok) {
        std::cerr << e <<" = "<< elements[pos] <<" in "<< slice_of[e] <<" : "
                  << slc.left <<" <= "<< pos <<" < "<< slc.right
              <<std::endl;
    }
    return ok;
}

void partition::check_all() {
    for (uint e : elements) assert(check_element(e, false));
}

template<typename C>
void partition::split_left(const C &container) {
    for (typename C::const_iterator it = container.cbegin();
         it != container.cend(); ++it) {
        elt e = *it;
        const uint s_e = slice_of[e];
        if (slices[s_e].child == not_a_slice) {
            const slice &parent = slices[s_e];
            if (parent.left + 1 >= parent.right // singleton
                && s_e != 0) // for partition::count_split_elements
                { continue; }
            slices.emplace_back(parent.left, parent.left, not_a_slice); // Can make parent invalid reference !
            slices[s_e].child = slices.size() - 1;
            split_slices.push_back(s_e);
        }
        slice &parent = slices[s_e];
        uint pos = pos_of[e];
        swap(pos, parent.left);
        (parent.left)++; // removes e from parent
        assert(&parent == &(slices[s_e]) || false);
        slices[parent.child].right++; // now child contains e
        slice_of[e] = parent.child;
    }
    for (i_slice s : split_slices) {
        slice &parent = slices[s];
        if (parent.left < parent.right) { // not empty
            ++nb_slices; // count the child
            assert(nb_slices <= _n);
        }
        parent.child = not_a_slice;
    }
    split_slices.clear();
}

void partition::print_debug() {
    std::vector<slice> slices_copy(slices);
    for (uint i = 0; i < slices_copy.size(); ++i) {
        slices_copy[i].child = i; // ignore original, recall index
    }
    std::sort(slices_copy.begin(), slices_copy.end(),
              [](const slice & s, const slice & t){
                  if (s.left == t.left) return s.right < t.right;
                  return s.left < t.left;
              });
    uint prev_right = 0, sum = 0;
    for (const slice &sl : slices_copy) {
        std::cerr << sl.child // slice index
                  <<"["<< sl.left <<","<< sl.right <<"): ";
        for (uint i=sl.left; i < sl.right; ++i) {
            ++sum;
            std::cerr << elements[i] <<" ";
        }
        std::cerr <<" |  ";
        assert(sl.left >= prev_right);
        assert(sl.left <= sl.right);
        prev_right = sl.right;
    }
    assert(sum == _n);
}

uint partition::nb_parts_debug() {
    uint p = 0;
    for (uint i = 0; i < _n ; ) {
        elt e = elements[i];
        ++p;
        i = slices[slice_of[e]].right; // first index in next part
    }
    return p;
}

// Same as split_left(), but succeeds only if all parts are split into two.
template<typename C>
bool partition::split_left_all(const C &container) {
    bool fail = false;
    for (typename C::const_iterator it = container.cbegin();
         it != container.cend(); ++it) {
        elt e = *it;
        uint s_e = slice_of[e];
        slice &parent = slices[s_e];
        if (parent.child == not_a_slice) {   
            parent.child = 0; // count number of elements removed from the part
            split_slices.push_back(s_e);
        }
        parent.child++;
        if (parent.left + parent.child >= parent.right) { // part becomes empty
            fail = true;
            break;
        }
    }
    fail = fail || split_slices.size() < nb_slices;
    // Clean up :
    for (i_slice s_p : split_slices) {
        slice &parent = slices[s_p];
        parent.child = not_a_slice;
    }
    split_slices.clear();
    if (fail) { // undo all splitting
        return false;
    } else { // do split
        split_left(container);
        return true;
    }
}

template bool partition::split_left_all<std::vector<unsigned int, std::allocator<unsigned int> > >(std::vector<unsigned int, std::allocator<unsigned int> > const&) ;

void partition::unslice_left(i_slice s) { // asserts the parent slice is right after
    slice &child = slices[s];
    assert(child.right < _n);
    i_slice p = slice_of[elements[child.right]];
    slice &parent = slices[p];
    assert(parent.left == child.right);
    parent.left = child.left;
    for (uint i = child.left; i < child.right; ++i) {
        elt e = elements[i];
        slice_of[e] = p;
    }
}

template<typename C>
void partition::unsplit_left_all(const C &container) {
    assert(nb_slices == uint(1) << log_2(nb_slices)); // nb_slices is a power of 2
    // unslice the last nb_slices/2 slices:
    uint nb_half = nb_slices >> 1;
    for (uint s = slices.size() - nb_half; s < slices.size(); ++s) {
        unslice_left(s);
    }
    slices.resize(slices.size() - nb_half);
    nb_slices = nb_half;
}

template void partition::unsplit_left_all<std::vector<unsigned int, std::allocator<unsigned int> > >(std::vector<unsigned int, std::allocator<unsigned int> > const&);

bool partition::has_singleton() {
    for (const slice & s : slices) {
        if (s.right == s.left + 1) return true;
    }
    return false;
}

uint partition::nb_parts_before(uint e) {
    uint nb = 0;
    for (const slice & s : slices) {
        if (s.right <= e && s.left < s.right) ++nb;
    }
    return nb;
}

void partition::swap(uint i, uint j) {
    if (i != j) {
        std::swap(elements[i], elements[j]);
        pos_of[ elements[i] ] = i;
        pos_of[ elements[j] ] = j;
    }
}

void partition::swap_front(elt e) {
    swap(pos_of[e], 0);
}

uint partition::dual_vcdim_lower_bound(const set_system& h,
                                       const set_sets& subsyst) {
    assert(h.nb_elts() == _n);
    init();

    uint n_split = 0, lb = 0;
    // Classes of elements:
    for (set s : subsyst) {
        split_left(h[s]);
        ++n_split;
        lb = std::max(lb, dual_vcdim_lower_bound(n_split));
    }

    return lb;
}

uint partition::dual_vcdim_lower_bound(uint nb_split) {
    uint n = nb_split;
    uint p = nb_parts();
    //double lb = std::ceil(std::log(double(p-1)) / std::log(double(n)));

    uint sum = 0, dmax = std::min(log_2(_n), n);
    //std::cerr <<"dmax="<< dmax <<" p="<< p <<std::endl;
    for (uint d = 0; d < dmax; ++d) {
        if (n < binomials[d].size()) {
            sum += binomials[d][n];
        } else {
            sum = _n;
        }
        if (p <= sum) return d;
        assert(d < dmax);
    }
    return dmax;
}

void partition::init_binomials() {
    const uint logn = log_2(_n), p_max = _n;
    binomials.resize(logn+1);
    for (uint d = 0; d <= logn; ++d) {
        if (d <= 1) {
            binomials[d].resize(_n+1);
        } else {
            // We are not interested by binomials greater than p_max.
            double pdth = std::pow(double(p_max), 1./double(d)); // p_max^{1/d}
            uint n_max = d * uint(pdth);
            binomials[d].resize(n_max+1);
        }
    }
    for (uint n = 0; n <= _n; ++n) { binomials[0][n] = 1; }
    for (uint d = 1; d <= logn; ++d) {
        binomials[d][d] = 1;
        for (uint n = d+1; n <= binomials[d].size() - 1; ++n) {
            binomials[d][n] = binomials[d-1][n-1] + binomials[d][n-1];
            if (binomials[d][n] > p_max) binomials[d][n] = p_max;
        }
    }
}

bool partition::dual_shattered(const set_system& h,
                               const set_sets& subsyst) {
    assert(h.nb_elts() == _n);
    init();

    if (subsyst.size() > log_2(nb_elts() + 1)) return false;

    uint p = nb_parts(); // 1 for the moment
    assert(p == 1);
    
    // Classes of elements:
    for (set s : subsyst) {
        if ( ! split_left_all(h[s])) return false;
        p <<= 1; // there should be twice more parts
        assert(nb_parts() == p);
    }

    return true;
}

std::ostream& operator<<(std::ostream & os, const partition & p) {
    os << "{ ";
    // scan elements:
    for (elt e : p.elements) {
        if (p.pos_of[e] == p.slices[ p.slice_of[e] ].left
            && p.pos_of[e] != 0) {
            os <<"| "; // delimiter
        }
        os << e <<" ";
    }    
    os << "}\n";
    return os;
}




void partition::unit() {
    std::cout <<"------------------- partition::unit --------------------\n";
    partition p(16);
    std::vector<uint>
        a { 1, 2, 3, 4, 5, 7},
        b { 4, 5, 8, 9 },
        c { 2, 4, 6, 8 },
        d { 1, 4, 7 };
    std::cout << p;
    p.check_all();
    p.split_left(a); std::cout << p;
    p.split_left(b); std::cout << p;
    p.check_all();
    p.split_left(c); std::cout << p;
    p.split_left(d); std::cout << p;
    p.check_all();

    uint dim = 4;
    set_system all = set_system::power_set(dim);
    set_sets all_elts;
    for (elt e : all.elements()) all_elts.push_back(e);
    uint lb = p.dual_vcdim_lower_bound(all.dual(), all_elts);
    std::cout << p;
    std::cout << "dual_vcdim_lower_bound: " << lb <<" vs "<< dim << std::endl;
    assert(lb == dim);
    assert(p.dual_shattered(all.dual(), all_elts));

    graph g;
    std::vector<std::vector<int> > edges = {
        {0, 1}, {0, 2}, {1, 2}, {0, 3}, {1, 4}, {2, 5}, {3,6},
        {7, 0}, {7, 1}, {8, 1}, {8, 2}, {9, 2}, {9, 0}
    };
    for (auto pair : edges) { g.add_edge(pair[0], pair[1]); }
    std::cout << g;
    partition p2(g.nb_nodes());
    set_elts shat{ 0, 1, 2 };
    bool shattered = p2.dual_shattered(g, shat);
    assert(shattered);
    std::cout <<"shattered = "<< shattered <<"\n";
    std::cout << p2;
    std::cout <<"split elements : ";
    for (node u : p2) { std::cout << u <<" "; }
    std::cout << std::endl <<"--\n";
    p2.init();
    for (node u : shat) {
        assert(p2.split_left_all(g.neighbors(u)));
        assert( ! p2.split_left_all(g.neighbors(9)));
        std::cout << u <<" : "<< p2;
    }
    for (auto it = shat.crbegin(); it != shat.crend(); ++it) {
        std::cout << *it <<" : ";
        p2.unsplit_left_all(g.neighbors(*it));
        std::cout << p2;
    }
}
