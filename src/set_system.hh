/** 
 * Growable set system (or hypergraph) where sets (or hyperedges) are indexed 
 * from 0 to n-1, and elements are indexes from 0 to p-1.
 * By set, we mean multiset as we do not detect duplicate elements, and a set
 * can contain multiple copies of the same element.
 * It is implemented as a vector of vectors.
 *
 * Basic example:
 *
#include <sstream>
#include "set_system.hh"

    set_system h;
 
    h.add(0, 1); // add to set 0 the element 1
    h.add(0, 2); 
    h.add(1, 2);
    
    for (set s : h.sets()) {
        std::cout << s <<" : { ";
        for (elt e : h[s]) { std::cout << e <<" "; } 
        std::cout <<"}\n";
    }

    std::istringstream inp("# a set system:\n"
                           "2 1\n 2 3\n 2 4\n"
                           "3 2\n 3 3\n"
                           );
    inp >> h;
    std::cout << h;
 *
 *
 * Outputs:
 *
0 : { 1 2 }
1 : { 2 }
{
  0 : { 1, 2 }
  1 : { 2 }
  2 : { 1, 3, 4 }
  3 : { 2, 3 }
}
 *
 */

#pragma once

#include <cassert>
#include <vector>
#include <iostream>

#include "integers.hh"


using set = uint;
using elt = uint;
using set_elts = std::vector<elt>;
using set_sets = std::vector<set>;

// input/output helpers:
std::ostream& operator<<(std::ostream & os, const set_elts & se) ;
std::istream& operator>>(std::istream & is, set_elts & se) ;

// friend operators:
class set_system;
std::ostream& operator<<(std::ostream & os, const set_system & r) ;
std::istream& operator>>(std::istream & is, set_system & r) ;


class set_system {

private:

    std::vector<set_elts> setelts; // the set system
    uint _n; // number of sets
    uint _e; // number of elements
    uint _m; // sum of set sizes

public:

    set_system() : _n(0), _e(0), _m(0) {}

    uint nb_sets() const { return _n; }
    uint nb_elts() const { return _e; }
    uint sum_set_sizes() const { return _m; }
    uint set_size(set s) const { return (uint) setelts[s].size(); }
    void set_reserve(set s, uint sz) { setelts[s].reserve(sz); }
    
    void add_set(set s) {
        if (s >= _n) {
            _n = s + 1;
            setelts.resize(_n);
        }
    }

    set new_set() {
        set s = nb_sets();
        add_set(s);
        return s;
    }

    void add_elt(elt e) {
        if (e >= _e) {
            _e = e + 1;
        }
    }

    void add(set s, elt e) {
        add_elt(e);
        add_set(s);
        setelts[s].push_back(e);
        ++_m;
    }

    void clear(uint n = 0) {
        _n = n; _e = 0; _m = 0;
        setelts.resize(_n);
        for (set_elts & s : setelts) s.clear();
    }


    template<typename C> // container of elt
    void append(set s, const C &more_elts) {
        for (elt e : more_elts) { add(s, e); }
    }


    set_elts& operator[](set s) {
        assert(s < _n);
        return setelts[s];
    }

    const set_elts& operator[](set s) const {
        assert(s < _n);
        return setelts[s];
    }

    // Return the dual set system where sets become elements,
    // and an element becomes the hyperedge of sets that contain it.
    set_system dual() const ;

    static set_system power_set(uint n) ;

    // Iterate over indexes of sets:
    using iterator = std::vector<set_elts>::const_iterator;
    iterator begin() const { return setelts.cbegin(); }
    iterator end() const { return setelts.cend(); }
    uint_range sets() const { return uint_range(0, _n); }
    uint_range elements() const { return uint_range(0, _e); }

    // Print a set system as in the comment at the top.
    friend std::ostream& operator<<(std::ostream & os, const set_system & r) ;

    // Read a set system as pairs 'set elt', one per line.
    friend std::istream& operator>>(std::istream & is, set_system & r) ;

    static void unit(); // unit test

};


