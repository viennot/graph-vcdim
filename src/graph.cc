#include <cassert>
#include<vector>
#include <algorithm>
#include<sstream>
#include<fstream>
#include <random>

#include "graph.hh"
#include "distrib.hh"
#include "partition.hh"

graph::graph(std::istream& is) : set_system() {
    const node node_max = std::numeric_limits<node>::max();
    uint m = 0;
    while ( ! is.eof()) {
        is >> std::ws; // white space
        if (is.eof()) break;
        std::string line;
        std::getline(is, line);
        while (line.size() > 0
               && (line[0]=='#' || line[0]=='c' || line[0]=='p')) { // comment
            if (is.eof()) break;
            std::getline(is, line);
        }
        if (is.eof()) break;
        ++m;
        std::istringstream iss(line);
        std::string u_s, v_s, wgt_s;
        iss >> std::ws;
        if (iss.eof()) continue; // blank line
        iss >> u_s >> std::ws;
        assert( ! iss.eof()); // bad line format for an edge: 2 ints expected
        iss >> v_s >> std::ws;
        if ( ! iss.eof()) { iss >> wgt_s >> std::ws; } // ignored weight as 3rd
        assert(iss.eof()); // bad line format for an edge: more than 2-3 ints
        auto u_i = std::stoll(u_s);
        assert(u_i >= 0 && std::uint64_t(u_i) <= std::uint64_t(node_max));
        auto v_i = std::stoll(v_s);
        assert(v_i >= 0 && std::uint64_t(v_i) <= std::uint64_t(node_max));
        add_edge(u_i, v_i);
    }
    for (node u : nodes()) {
        if (set_size(u) == 0) {
            add(u, u);
        }
    }
    stats();
    std::cerr <<"  ("<< m <<" edges read)\n";
    assert(m == nb_edges());
}

graph erdos_renyi_random_graph(uint n, float p, int seed) {
    //std::random_device rand_dev;
    std::mt19937 rand_gen(seed);
    std::uniform_real_distribution<> unif(0.0, 1.0);
    // generate graph :
    graph g;
    for (uint u=0; u<n; ++u) { g.add_node(u); }
    for (uint u=0; u<n; ++u) {
         for (uint v=0; v<u; ++v) {
              if (unif(rand_gen) < p) {
                   g.add_edge(u, v);
              }
         }
    }
    g.stats();
    return g;
}

graph power_law_random_graph(uint n, float beta, int seed,
                             bool no_self_loop) {
    // power law with parameter beta :
    std::vector<double> p_deg(n);
    double nf = n;
    for (uint d=1; d<n; ++d) p_deg[d] = nf / pow((float)d, beta);
    p_deg[0] = 0.;
    std::discrete_distribution<> power_law(p_deg.begin(), p_deg.end()); // normalization is done by std::discrete_distribution
    // generate degrees :
    std::mt19937 rand_gen(seed);
    std::vector<uint> deg(n);
    uint m = 0;
    for(uint u=0; u<n; ++u) {
        deg[u] = power_law(rand_gen);
        m += deg[u];
    }
    // generate edges :
    std::vector<uint> dst, src;
    dst.reserve(m); src.reserve(m);
    for (uint u=0; u<n; ++u) {
        for (uint e=0; e<deg[u]; ++e) {
            dst.push_back(u); src.push_back(u);
        }
    }
    std::uniform_int_distribution<> rnd_int(0, m-1);
    for (uint e=m-1; e>0; --e) {
        uint f = rnd_int(rand_gen) % (e+1);
        std::swap(dst[e], dst[f]);
    }
    // avoid self edge :
    if (no_self_loop) {
        for (uint u=0, e=0; u<n; ++u) {
            for (uint i=0; i<deg[u]; ++i, ++e) {
                while (u == dst[e]) {
                    uint f = rnd_int(rand_gen) % m;
                    while (u == dst[f] || src[f] == dst[e]) {
                        f = rnd_int(rand_gen) % m;
                    }
                    std::swap(dst[e], dst[f]);
                }
            }
        }
    }
    // generate graph :
    graph g;
    for (uint u=0; u<n; ++u) { g.add_node(u); }
    for (uint u=0, e=0; u<n; ++u) {
        for (uint i=0; i<deg[u]; ++i, ++e) g.add_edge(u, dst[e]);
    }
    g.stats();
    return g;
}


void graph::stats() const {
    uint isolated = 0;
    for (node u : nodes()) {
        assert(set_size(u) > 0);
        if (set_size(u) == 1) ++isolated;
    }
    std::cerr <<"n="<< nb_nodes() <<" m="<< nb_edges();
    if (isolated > 0) std::cerr <<" isolated_nodes=" << isolated;
    std::cerr <<"\n  degrees: ";
    distrib(nb_nodes(), [this](size_t i) {return degree(i);}).print(std::cerr);
    //std::cerr <<"  degeneracy: "<< degeneracy() <<"\n";
}

graph graph::sort() const { // 2.50s on as-skitter
    graph g;
    g.add_set(nb_nodes() - 1);
    for (node u : nodes()) {
        g.set_reserve(u, degree(u) + 1); // 5.3s without that
    }
    for (node u : nodes()) {
        for (node v : neighbors(u)) {
            g.add(v, u);
        }
    }
    return g;
}

graph graph::simple() const { 
    graph g;
    g.add_set(nb_nodes() - 1);
    for (node u : nodes()) {
        g.set_reserve(u, degree(u) + 1); 
    }
    for (node u : nodes()) {
        for (node v : neighbors(u)) {
            if (g[v].size() == 0 || u != g[v].back()) {
                g.add(v, u);
            }
        }
    }
    return g;
}

std::vector<edge> graph::edges(bool simple, bool sort) const { // 3.4s on as-skitter with simple=false, sort=true
    std::vector<edge> edg;
    edg.reserve(simple ? nb_edges() : 2 * nb_edges() + nb_nodes());
    for (node u : nodes()) {
        for (node v : neighbors(u)) {
            if ( ( ! simple) || u < v)
                edg.emplace_back(u, v); // v, u -> 4.7s-6.5s, 2.6s-3.7s just tail sorting
        }
    }
    if (sort) { // true in time test, 2.8s without
        std::sort(edg.begin(), edg.end(),
                  [](const edge e, const edge f) {
                      return e.tail < f.tail
                                      || (e.tail == f.tail && e.head < f.head);
                  });
    }
    return edg;
}

bool graph::operator==(const graph & g) const {
    std::vector<edge> edg = edges(false, true);
    std::vector<edge> oth = g.edges(false, true);
    return edg == oth;
}

bool graph::is_symmetric() const {
    return operator==(sort());
}

bool graph::is_simple() const { 
    graph g = sort();
    for (node u : nodes()) {
        node prev(-1);
        for (node v : g.neighbors(u)) {
            if (prev != uint(-1)) {
                if (v == prev) return false;
                assert(v > prev);
            }
            prev = v;
        }
    }
    return true;
}


void graph::dfs_ordering(std::vector<node>::iterator it, node u) const {
    std::vector<node> visited(nb_nodes(), false);
    it = dfs_ordering(it, u, visited);
    for (node u : nodes()) {
        if ( ! visited[u]) it = dfs_ordering(it, u, visited);
    }
}

std::vector<node>::iterator
graph::dfs_ordering(std::vector<node>::iterator it, node u,
                    std::vector<node> & visited) const {
    *(it++) = u;
    visited[u] = true;
    for (node v : neighbors(u)) {
        if ( ! visited[v]) it = dfs_ordering(it, v, visited);
    }
    return it;
}

graph graph::induced_subgraph(const std::vector<node> & set,
                              std::vector<node> & mapping) const {
    mapping.resize(set.size());
    std::vector<node> mapping_inv(nb_nodes(), node(-1));
    for (uint i=0; i < set.size(); ++i) {
        mapping[i] = set[i];
        mapping_inv[set[i]] = i;
    }
    
    graph h;
    h.add_node(set.size()-1);
    for (node u : set) {
        assert(u < nb_nodes());
        node ui = mapping_inv[u];
        assert(ui < h.nb_nodes());
        h.add_node(ui);
        for (node v : neighbors(u)) {
            node vi = mapping_inv[v];
            if (vi < set.size() && ui < vi) { h.add_edge(ui, vi); }
        }
    }
    h.stats();

    return h;
}

std::vector<node> graph::twin_classes() const {
    partition part(nb_nodes());
    for (node u : nodes()) { part.split_left(neighbors(u)); }
    std::cerr << nb_nodes() - part.nb_parts() <<" twins\n";
    std::vector<node> set;
    set.reserve(part.nb_parts());
    for (uint i = 0; i < part.nb_elts() ; ) {
        node u = part[i];
        set.push_back(u);
        auto [ left, right ] = part.part_bounds(u);
        i = right; // first index in next part
    }
    return set;
}

uint graph::degeneracy () const {
    // We will have to update degrees while removing nodes:
    std::vector<uint> deg(nb_nodes());
    uint max_deg = 0;
    int64_t sum_deg = 0;
    for (node u : nodes()) {
        uint d = degree(u);
        if (d > max_deg) { max_deg = d; }
        deg[u] = d;
        sum_deg += d;
    }
    
    double avg_deg = double(sum_deg) / double(nb_nodes());
    double max_avg_deg = avg_deg;

    std::vector<std::vector<node>> nodes_by_deg(max_deg + 1);
    std::vector<uint> index(nb_nodes());
    for (node u : nodes()) {
        uint d = deg[u];
        index[u] = nodes_by_deg[d].size();
        nodes_by_deg[d].push_back(u);
    }

    uint k = 0;
    for (uint i = 0; i < nb_nodes(); ) {
        while (nodes_by_deg[k].size() == 0) ++k;
        assert(k <= max_deg);
        //std::cerr << k <<"-core size: "<< nb_nodes() - i <<"\n";
        std::vector<node> & deg_k = nodes_by_deg[k]; // nodes with deg <= k
        while (deg_k.size() > 0) {
            double avg_deg = double(sum_deg) / double(nb_nodes() - i);
            if (avg_deg > max_avg_deg) { max_avg_deg = avg_deg; }
            node u = deg_k.back();
            ++i;
            deg_k.pop_back();
            sum_deg -= 2*deg[u];
            for (node v : neighbors(u)) {
                uint d = deg[v];
                if (d <= k) {
                    // node has deg <= k, do nothing
                } else {
                    uint i = index[v];
                    node w = nodes_by_deg[d].back();
                    std::swap(nodes_by_deg[d][i], nodes_by_deg[d].back());
                    assert(nodes_by_deg[d][i] == w);
                    index[w] = i;
                    assert(nodes_by_deg[d].back() == v);
                    nodes_by_deg[d].pop_back();
                    --d; // after removing u, deg of v decreases by 1
                    deg[v] = d;
                    index[v] = nodes_by_deg[d].size();
                    nodes_by_deg[d].push_back(v);
                }
            }
        }
    }

    std::cerr <<"avg_deg: "<< avg_deg <<"\n";
    std::cerr <<"max_avg_deg: "<< max_avg_deg <<" (2-approximation)\n";

    return k;

}

std::vector<uint> graph::kcore_ranking (bool verbose) const {
    // We will have to update degrees while removing nodes:
    std::vector<uint> deg(nb_nodes());
    uint max_deg = 0;
    for (node u : nodes()) {
        uint d = degree(u);
        if (d > max_deg) { max_deg = d; }
        deg[u] = d;
    }

    std::vector<std::vector<node>> nodes_by_deg(max_deg + 1);
    std::vector<uint> index(nb_nodes());
    for (node u : nodes()) {
        uint d = deg[u];
        index[u] = nodes_by_deg[d].size();
        nodes_by_deg[d].push_back(u);
    }

    std::vector<uint> rank(nb_nodes());
    uint k = 0, kmax = 0;
    for (uint i = 0; i < nb_nodes(); ) {
        while (k <= max_deg && nodes_by_deg[k].size() == 0) ++k;
        assert(k <= max_deg);
        if (k > kmax) { kmax = k; }
        if (verbose) std::cerr << k <<"-core size: "<< nb_nodes() - i <<"\n";
        std::vector<node> & deg_k = nodes_by_deg[k]; // nodes with deg <= k
        while (deg_k.size() > 0) {
            node u = deg_k.back();
            rank[u] = i;
            ++i;
            deg_k.pop_back();
            for (node v : neighbors(u)) {
                if (v == u) continue;
                uint d = deg[v];
                assert(d > 0);
                if (d <= k) {
                    // node has deg <= k, do nothing
                } else {
                    uint i = index[v];
                    node w = nodes_by_deg[d].back();
                    std::swap(nodes_by_deg[d][i], nodes_by_deg[d].back());
                    assert(nodes_by_deg[d][i] == w);
                    index[w] = i;
                    assert(nodes_by_deg[d].back() == v);
                    nodes_by_deg[d].pop_back();
                    --d; // after removing u, deg of v decreases by 1
                    deg[v] = d;
                    index[v] = nodes_by_deg[d].size();
                    nodes_by_deg[d].push_back(v);
                }
            }
        }
    }

    std::cerr <<"Degeneracy: "<< kmax <<"\n";

    return rank;

}


std::vector<uint> graph::kcore_ranking (const std::vector<uint> &prerank) const {
    std::vector<uint> prenodes(nb_nodes());
    for (node u : nodes()) { prenodes[u] = u; }
    std::sort(prenodes.begin(), prenodes.end(),
                  [&prerank](const uint u, const uint v) {
                      return prerank[u] < prerank[v];
                  });
    
    // We will have to update degrees while removing nodes:
    std::vector<uint> deg(nb_nodes());
    uint max_deg = 0;
    for (node u : nodes()) {
        uint d = degree(u);
        if (d > max_deg) { max_deg = d; }
        deg[u] = d;
    }

    std::vector<std::vector<node>> nodes_by_deg(max_deg + 1);
    std::vector<uint> index(nb_nodes());
    std::vector<bool> seen(nb_nodes(), false);

    std::vector<uint> rank(nb_nodes());

    for (uint i_rank = 0, i_pre = 0; i_pre < nb_nodes(); ) {

        uint k = max_deg; // current smallest degree is the kcore value
        uint k_max = 0;
        
        // consider only nodes with same prerank
        uint rank_pre = prerank[prenodes[i_pre]];
        uint nb_nodes_round = 0;
        for ( ; i_pre < nb_nodes() ; ++i_pre) {
            uint u = prenodes[i_pre];
            if (prerank[u] != rank_pre) break;
            ++nb_nodes_round;
            uint d = deg[u];
            if (d < k) { k = d; }
            if (d > k_max) { k_max = d; }
            index[u] = nodes_by_deg[d].size();
            nodes_by_deg[d].push_back(u);
        }

        for (uint i = 0; i < nb_nodes_round; ) {
            while (k <= k_max && nodes_by_deg[k].size() == 0) ++k;
            assert(k <= max_deg);
            //std::cerr << k <<"-core size: "<< nb_nodes() - i <<"\n";
            std::vector<node> & deg_k = nodes_by_deg[k]; // nodes with deg <= k
            while (deg_k.size() > 0) {
                node u = deg_k.back();
                assert( ! seen[u]); seen[u] = true;
                rank[u] = i_rank++;
                ++i;
                deg_k.pop_back();
                for (node v : neighbors(u)) {
                    if (v == u) continue;
                    uint d = deg[v];
                    assert(d > 0);
                    if (d <= k || prerank[v] != rank_pre) {
                        deg[v] = d-1;//after removing u, deg of v decreases by 1
                    } else {
                        uint i = index[v];
                        node w = nodes_by_deg[d].back();
                        std::swap(nodes_by_deg[d][i], nodes_by_deg[d].back());
                        assert(nodes_by_deg[d][i] == w);
                        index[w] = i;
                        assert(nodes_by_deg[d].back() == v);
                        nodes_by_deg[d].pop_back();
                        --d; // after removing u, deg of v decreases by 1
                        deg[v] = d;
                        index[v] = nodes_by_deg[d].size();
                        nodes_by_deg[d].push_back(v);
                    }
                }
            }
        }
        for (uint d = 0; d <= max_deg; ++d) assert(nodes_by_deg[d].size() == 0);

    }
    
    return rank;

}


void graph::unit() {
    std::cout <<"------------------- graph::unit --------------------\n";

    graph g;
    std::vector<std::vector<int> > edges = {
        {0, 1}, {0, 2}, {1, 2}, {0, 3}, {1, 4}, {2, 5}, {3,6} 
    };
    for (auto pair : edges) { g.add_edge(pair[0], pair[1]); }
    std::cout << g;
    g.stats();

    assert(g == g.simple());
    assert(g.is_symmetric());
    assert(g.is_simple());
    graph g2 = g.sort();
    std::cout << g2;
    assert(g2.edges(false, false) == g.edges(false, true));
    for (auto pair : edges) { if (pair[0] != 0) g2.add_edge(pair[1], pair[0]); }
    std::cout << g2;
    assert(g2.is_symmetric());
    assert( ! g2.is_simple());
    g2 = g2.simple();
    assert(g2.is_symmetric());
    assert(g2.is_simple());
    assert(g2 == g);

    uint k = g.degeneracy();
    std::cout <<"Degeneracy : "<< k <<"\n";
    assert(k == 2);

    std::vector<uint> rank = g.kcore_ranking();
    for (node u : g.nodes()) {
        uint ku = 0;
        for (node v : g.neighbors(u)) {
            if (rank[v] > rank[u]) { ++ku; }
        }
        assert(ku <= k);
    }

    std::vector<node> mapping{0, 1, 2, 3, 4, 5, 6};
    set_elts subset{4, 1, 5, 2};
    graph h = g.induced_subgraph(subset, mapping);
    std::cout <<"subgraph induced by "<< subset <<":\n"<< h;
    for (node u : h.nodes()) {
        std::cout << u <<" is "<< mapping[u] <<" :";
        for (node v : h.neighbors(u)) std::cout <<" "<< mapping[v];
        std::cout<<"\n";
    }
    set_elts subset2{1, 0, 3};
    graph h2 = h.induced_subgraph(subset2, mapping);
    std::cout <<"subgraph induced by "<< subset2 <<":\n"<< h2;
    for (node u : h2.nodes()) {
        std::cout << u <<" is "<< mapping[u] <<" :";
        for (node v : h2.neighbors(u)) std::cout <<" "<< mapping[v];
        std::cout<<"\n";
    }

    std::cout <<"unit_data/BIOGRID graph : \n";
    std::ifstream ifs("../unit_data/BIOGRID-SYSTEM-Dosage_Rescue-3.4.145.bcc.edgelist");
    graph bio(ifs);
    ifs.close();
    std::cout <<"N[123] = "<< bio.neighbors(123) << std::endl;
    assert(bio.degree(123) == 3);

    uint kbio = bio.degeneracy();
    std::cout <<"Degeneracy : "<< kbio <<"\n";
    assert(kbio == 7);

    std::vector<uint> rankbio = bio.kcore_ranking();
    uint kmax = 0;
    for (node u : bio.nodes()) {
        uint ku = 0;
        for (node v : bio.neighbors(u)) {
            if (rankbio[v] > rankbio[u]) { ++ku; }
        }
        assert(ku <= kbio);
        kmax = std::max(kmax, ku);
    }
    assert(kmax == kbio);
}
