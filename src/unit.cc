#include <fstream>
#include <sstream>
#include <vector>
#include <iostream>

#include "set_system.hh"
#include "partition.hh"
#include "graph.hh"
#include "vcdim.hh"

int main (int argc, char **argv) {

    set_system::unit();
    partition::unit();
    graph::unit();
    vcdim_unit();
    
}
