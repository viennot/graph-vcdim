#include <cassert>
#include <iostream>
#include <fstream>
#include <sstream>
#include <unordered_map>
#include <vector>


std::size_t index_file(std::string fname) { // each line is seen as a seq. of words
    std::unordered_map<std::string,std::size_t> word_index;
    std::vector<std::string> word_list;
    std::size_t n = 0L; // the number of distinct words seen

    auto index = [&word_index, &word_list, &n](const std::string & s) {
        if (word_index.find(s) == word_index.end()) {
            word_index[s] = n;
            word_list.push_back(s);
            return n++;
        } else {
            return word_index[s];
        }
    };
    
    std::ifstream is(fname);
    std::ofstream os(std::string(fname) + ".index");

    while ( ! is.eof()) {
        // read a line:
        is >> std::ws;
        std::string line;
        std::getline(is, line);

        // comment :
        while (line.size() > 0 && line[0] == '#') {
            if (is.eof()) goto endloop;
            std::getline(is, line);
        }

        // read words and output their indexes:
        std::istringstream iss(line);
        iss >> std::ws;
        std::string word;
        bool first = true;
        while ( ! iss.eof()) {
            iss >> word >> std::ws;
            if (first) {
                first = false;
            } else {
                os << " ";
            }
            os << index(word);
        }
        os << "\n";
    }
 endloop:
    is.close();
    os.close();

    // Output word list.
    os.open(std::string(fname) + ".strings");
    for (std::size_t i = 0; i < n; ++i) {
        os << i <<" "<< word_list[i] << std::endl;
    }
    os.close();

    return n;
}
