/** index strings in a file */

#pragma once

#include <string>

/** Read strings from a file f and produce two files:
 * - f.index with the same line structure where each string is replaced by an 
 *   integer from 0 to n-1 where n is the number of strings found in the file,
 * - f.strings with one index and one string per line.
 * Returns n.
 */

std::size_t index_file(std::string fname) ;

