// Author: Laurent Viennot, Inria, 2022.

/** Partition refinement implementation :
    Starting from a parition P of elements, it is refined by pivoting on a set S
    by splitting each part Q in P into Q \cap S and Q - S.
    Elements must be uints from 0 to n-1.
 */

#pragma once

#include <vector>
#include <iterator>
#include <fstream>
#include <tuple>

#include "integers.hh"
#include "set_system.hh"

using uint = std::uint_least32_t;

class partition;

std::ostream& operator<<(std::ostream & os, const partition & p) ;


using elt = uint;


class partition {

private:

    std::vector<elt> elements;

    uint _n; // number of elements

    using i_slice = uint; // index of a slice

    static constexpr i_slice not_a_slice = std::numeric_limits<uint>::max();

    struct slice { // A slice of the partition, that is a part.
        uint left, right; // bounds in elements, left included, right excluded
        i_slice child; // When splitting a slice, it has child.
        slice() : left(0), right(0), child(not_a_slice) {}
        slice(uint l, uint r, i_slice c) : left(l), right(r), child(c) {}
    };

    uint nb_slices;

    std::vector<slice> slices;

    std::vector<i_slice> slice_of; // The slice containing an element.

    std::vector<i_slice> split_slices; // Recall which slices were split.
    
    std::vector<uint> pos_of; // The position of an element.

    static constexpr uint not_a_pos = std::numeric_limits<uint>::max();

    std::vector<std::vector<uint> > binomials; // for dual_vcdim_lower_bound


public:
    // Create a partition of n elements { 0,1,...,n-1 } with only one part
    // containing all elements.
    partition(uint n) ;

    // Put all elements in the same part.
    void init() ;

    // Total number of elements.
    uint nb_elts() { return _n; }

    // Split parts according to intersection with a set of elements.
    template<typename C>
    void split_left(const C &container) ;

    // Split parts according to intersection with a set of elements, succeeds only if all parts are split into two non empty parts. Returns true upon success. Otherwise, the partition is left untouched. 
    template<typename C>
    bool split_left_all(const C &container) ;

    // Undo a successfull split_left_all() (using same set of elements as argument).
    template<typename C>
    void unsplit_left_all(const C &container) ;

    // Returns true iff at least one part is a singleton.
    bool has_singleton() ;
    
    // Count how many elements have been moved by split overall. Equivalently, it is the size of the union of sets passed to split_left().
    uint count_split_elements() const { return slices[0].left; }

    // Size of the first part.
    uint first_part_size() const { return slices[slice_of[elements[0]]].right; }

    using iterator = std::vector<elt>::iterator;
    iterator begin() { return elements.begin(); }
    iterator end() { return elements.begin() + slices[0].left; } // only moved elements !
    elt operator[](uint i) {return elements[i]; }

    // Return the number of parts:
    uint nb_parts() { return nb_slices; }

    // Return part bounds of elt [e]:
    std::tuple<uint, uint> part_bounds(elt e) {
        const slice & s = slices[slice_of[e]];
        return { s.left, s.right };
    }

    // Return the number of parts before index [e]:
    uint nb_parts_before(uint e) ;

    // Lower bound of the dual VC dim for a subset of sets listed in subsyst.
       /* Note : After splitting each part P of the partition into P \cap S
        * and P - S for each set S in subsyst, we obtain a partition where
        * each part corresponds to a class of elements e contained by the same
        * sets of S. In other words, each part corresponds to a range in
        * the restriction of the dual to sets of subsyst. We can then use the
        * Perles-Sauer-Shelah bound r <= Sum_{0 <= i <= d} (n choose i) where r
        * is the number of ranges, d is the VC-dimension (of the dual), and n
        * is the number of sets in subsyst. We can also use this bound after
        * each splitting step with regards to the sub-system of sets of subsyst
        * considered for splitting so far.
        */
    uint dual_vcdim_lower_bound(const set_system &h,
                                const set_sets& subsyst) ;

    // Lower bound of the dual VC dim after splitting on nb_split sets.
    uint dual_vcdim_lower_bound(uint nb_split) ;
    
    // Test if the list subsyst of sets in subsyst is shattered in the dual.
    bool dual_shattered(const set_system& h,
                        const set_sets& subsyst) ;

    // Bring element e in position 0;
    void swap_front(const elt e) ;
    
    // Unit tests.
    static void unit() ;

    friend std::ostream& operator<<(std::ostream & os, const partition & p) ;

    
private:

    // Exchange elements in positions i and j.
    void swap(uint i, uint j) ;

    // Merge a slice the one right after (assumed to be its parent in the last split_left).
    void unslice_left(i_slice s) ;

    // Check an element position.
    bool check_element(uint e, bool verbose = true) ;
    
    // Check all elements.
    void check_all() ;

    // Compute binomials for dual_vcdim_lower_bound():
    void init_binomials() ;
    
    // OBSOLETE. Insert some elements in the unique part.
    template<typename C>
    void insert(const C &container) ;

    // Count parts by scanning [elements].
    uint nb_parts_debug() ;

    // Print all slices.
    void print_debug() ;

};
