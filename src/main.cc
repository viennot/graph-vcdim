#include <cassert>
#include <fstream>
#include <sstream>
#include <utility>
#include <algorithm>
#include <vector>
#include <unordered_map>
#include <cmath>

#include "index.hh"
#include "distrib.hh"
#include "set_system.hh"
#include "partition.hh"
#include "vcdim.hh"


// ----------- helper functions for reading args -----------------

void usage_exit (char **argv) {
    auto paragraph = [](std::string s, int width=80) -> std::string {
        std::string acc;
        while (s.size() > 0) {
            int pos = s.size();
            if (pos > width) pos = s.rfind(' ', width);
            std::string line = s.substr(0, pos);
            acc += line + "\n";
            s = s.substr(pos);
        }
        return acc;
    };
    
    std::cerr <<"\nUsage: "<< argv[0]
              <<" [options] commands with possible commands:\n"

              << paragraph ("vcdim [fname]: compute the VC dimension of the undirected graph in file [fname] (formatted as one edge per line).")
              << paragraph ("vcdimopt [ordering] [kcoreub] [localub] [reduce] [sparse] [ball2] [fname]: compute the VC dimension of the undirected graph in file [fname] (formatted as one edge per line) with the following parameters: ")
        << paragraph ("   [ordering] specifies the ordering for searching shattered sets among high degree nodes : 1 for kcore ordering, 2 for non-decr. degrees, 3 for non-decr. local upper bounds, 4 for random.")
        << paragraph ("   [kcoreub] specifies to use finer upper bounds based on analysing degrees with respect to a kcore elimination of nodes (1 for true, 0 otherwise).")
        << paragraph ("   [localub] specifies to use local upper bounds based on bounding shattered sets inside neighborhoods, these bounds are updated as we progress in the ordering. (1 for true, 0 otherwise).")
        << paragraph ("   [reduce] specifies to reduce the graph to high degree nodes plus a minimal set of nodes allowing to preserve traces of neighborhoods on high degree nodes (1 for true, 0 otherwise).")
              << paragraph ("   [sparse] specifies to first make a sparse scan of high deg nodes, 1 every lower_bound-1, to obtain tight local ub on these nodes (1 for true, 0 otherwise). ")
              << paragraph ("   [ball2] specifies to restrict the search of shattered sets X={x1,...} to high degree nodes in B(x1, 2), that is at distance at most 2 from the first node x1. ")
        
              << paragraph ("degeneracy [fname]: compute the degeneracy of the undirected graph in file [fname] (formatted as one edge per line).")

              << paragraph ("kcore [fname]: compute a kcore ranking of the undirected graph in file [fname] (formatted as one edge per line).")

              << paragraph ("vcdim-lb [fname] [maxvisits]: compute a lower bound of the VC dimension of the undirected graph in file [fname] (formatted as one edge per line), optional argument [maxvisits] (defaults to 64) indicates how many times a high deg node is visited in a pruned search of shattered sets among high degree nodes where only maxvisits/2 following a node are potentially to trial sets with that node.")

              << paragraph ("vcdim-lb-exact [fname]: compute the exact VC dimension (basic algorithm) of the undirected graph in file [fname] (formatted as one edge per line).")

              << paragraph ("shattered [fname] [elt1] [elt2] ... [eltk] : read the graph in file [fname] (formatted as one edge per line) and test if the set { [elt1], ..., [eltk] } is shattered.")

              << paragraph ("\nExample:")
              << paragraph ("_build/main vcdim unit_data/BIOGRID-SYSTEM-Dosage_Rescue-3.4.145.bcc.edgelist")
              << paragraph ("\n");
        exit(1);
}

std::vector<std::string> get_args(int argc, char **argv) {
    std::vector<std::string> a;
    for (int i = 1; i < argc; ++i) {
        std::string s(argv[i]);
        if (s[0] == '-') continue; // option
        a.push_back(s);
    }
    return a;
}

bool has_opt(int argc, char **argv, std::string opt) {
    assert(opt[0] == '-');
    for (int i = 0; i < argc; ++i) {
        std::string s(argv[i]);
        if (s == opt) return true;
    }
    return false;
}


// ---------------------------- main -------------------------


int main (int argc, char **argv) {

    // ------------------------ usage -------------------------
    std::vector<std::string> args = get_args(argc, argv);
    if (args.size() <= 1
        || has_opt(argc, argv, "-h") || has_opt(argc, argv, "--help")) {
        usage_exit(argv);
    }

    auto check_open = [](const std::ifstream &ifs){
        if ( ! ifs.is_open()) {
            std::cerr << "Could not open file!\n";
            exit(2);
        }
    };

    std::size_t i = 0; // arg index
    
    auto opt_uint_arg = [&i, &args](uint dft){
        if (i < args.size()) {
            return uint(std::stoi(args[i++]));
        }
        return dft;
    };

    // ------------------------ commands ---------------------
    while (i < args.size()) {

        std::string com = args[i++];

        if (com == "index") {
            std::cerr <<"index: "<< index_file(args[i++]) <<" strings found\n";
        } else if (com == "vcdim") {
            std::ifstream ifs(args[i++]);
            check_open(ifs);
            graph g(ifs);
            ifs.close();
            bool simple = g.is_simple();
            std::cerr <<"is simple: "<< simple <<"\n";
            if ( ! simple) {
                g = g.simple();
                assert(g.is_symmetric() && g.is_simple());
                g.stats();
            }
            vcdim vcg(g);
            ordering ord = ordering(opt_uint_arg(1));
            bool kcore_ubs = bool(opt_uint_arg(1));
            bool local_ubs = bool(opt_uint_arg(0));
            bool reduce = bool(opt_uint_arg(0));
            bool sparse = bool(opt_uint_arg(0));
            bool ball2 = bool(opt_uint_arg(1));
            std::cout << vcg.compute_vcdim(ord, kcore_ubs, local_ubs, reduce,
                                           sparse, ball2)
                      << std::endl;
        } else if (com == "vcdimopt") {
            ordering ord = ordering(std::stoi(args[i++]));
            bool kcore_ubs = bool(std::stoi(args[i++]));
            bool local_ubs = bool(std::stoi(args[i++]));
            bool reduce = bool(std::stoi(args[i++]));
            bool sparse = bool(std::stoi(args[i++]));
            bool ball2 = bool(std::stoi(args[i++]));
            uint maxvisits = uint(std::stoi(args[i++]));
            std::ifstream ifs(args[i++]);
            check_open(ifs);
            graph g(ifs);
            ifs.close();
            bool simple = g.is_simple();
            std::cerr <<"is simple: "<< simple <<"\n";
            if ( ! simple) {
                g = g.simple();
                assert(g.is_symmetric() && g.is_simple());
                g.stats();
            }
            vcdim vcg(g);
            std::cout << vcg.compute_vcdim(ord, kcore_ubs, local_ubs, reduce,
                                           sparse, ball2, maxvisits)
                      << std::endl;
        } else if (com == "vcdim-overhead") {
            ordering ord = ordering(std::stoi(args[i++]));
            bool kcore_ubs = bool(std::stoi(args[i++]));
            bool local_ubs = bool(std::stoi(args[i++]));
            bool reduce = bool(std::stoi(args[i++]));
            bool sparse = bool(std::stoi(args[i++]));
            bool ball2 = bool(std::stoi(args[i++]));
            uint maxvisits = uint(std::stoi(args[i++]));
            std::ifstream ifs(args[i++]);
            check_open(ifs);
            graph g(ifs);
            ifs.close();
            bool simple = g.is_simple();
            std::cerr <<"is simple: "<< simple <<"\n";
            if ( ! simple) {
                g = g.simple();
                assert(g.is_symmetric() && g.is_simple());
                g.stats();
            }
            vcdim vcg(g);
            std::cout << vcg.compute_vcdim(ord, kcore_ubs, local_ubs, reduce,
                                           sparse, ball2, maxvisits, true)
                      << std::endl;
        } else if (com == "cleangraph") {
            std::ifstream is(args[i++]);
            check_open(is);
            graph g;
    std::unordered_map<std::string,uint> vi; // vertex index
    std::vector<std::string> lab;
    const node node_max = std::numeric_limits<node>::max();
    uint n=0, m=0;
    while ( ! is.eof()) {
        is >> std::ws; // white space
        if (is.eof()) break;
        std::string line;
        std::getline(is, line);
        while (line.size() > 0
               && (line[0]=='#' || line[0]=='c' || line[0]=='p')) { // comment
            if (is.eof()) break;
            std::getline(is, line);
        }
        if (is.eof()) break;
        ++m;
        std::istringstream iss(line);
        std::string u_s, v_s, wgt_s;
        iss >> std::ws;
        if (iss.eof()) continue; // blank line
        iss >> u_s >> std::ws;
        assert( ! iss.eof()); // bad line format for an edge: 2 ints expected
        iss >> v_s >> std::ws;
        if ( ! iss.eof()) { iss >> wgt_s >> std::ws; } // ignored weight as 3rd
        assert(iss.eof()); // bad line format for an edge: more than 2-3 ints
        if (vi.count(u_s) == 0) {
            lab.push_back(u_s); assert(n < node_max); vi[u_s] = n++;
        }
        if (vi.count(v_s) == 0) {
            lab.push_back(v_s); assert(n < node_max); vi[v_s] = n++;
        }
        uint u_i = vi[u_s];
        uint v_i = vi[v_s];
        g.add_edge(u_i, v_i);
    }
            is.close();
            g = g.simple();
            std::cout <<"# "<< g.nb_nodes() <<" nodes "<< g.nb_edges() <<" edges\n";
            for (edge e : g.edges(true, true)) {
                std::cout << e.tail <<" "<< e.head << std::endl;
            }
        } else if (com == "vcdim-lb") {
            std::ifstream ifs(args[i++]);
            check_open(ifs);
            graph g(ifs);
            ifs.close();
            bool simple = g.is_simple();
            std::cerr <<"is simple: "<< simple <<"\n";
            if ( ! simple) {
                g = g.simple();
                assert(g.is_symmetric() && g.is_simple());
                g.stats();
            }
            vcdim vcg(g);
            uint maxvisits = opt_uint_arg(64);
            std::cout << vcg.greedy_vcdim_lb(maxvisits)
                      << std::endl;
        } else if (com == "vcdim-bounds") {
            std::ifstream ifs(args[i++]);
            check_open(ifs);
            graph g(ifs);
            ifs.close();
            bool simple = g.is_simple();
            std::cerr <<"is simple: "<< simple <<"\n";
            if ( ! simple) {
                g = g.simple();
                assert(g.is_symmetric() && g.is_simple());
                g.stats();
            }
            vcdim vcg(g);
            vcg.init_bounds(true);
            // true for printing:
            // max_loc_lb loc_ub 1+log(Delta) log(n) degen+1
        } else if (com == "read") {
            std::ifstream ifs(args[i++]);
            check_open(ifs);
            graph g(ifs);
            ifs.close();
            vcdim vcg(g);
            std::cout << "read" << std::endl;
        } else if (com == "vcdim-lb-opt") {
            uint maxvisits = uint(std::stoi(args[i++]));
            std::ifstream ifs(args[i++]);
            check_open(ifs);
            graph g(ifs);
            ifs.close();
            bool simple = g.is_simple();
            std::cerr <<"is simple: "<< simple <<"\n";
            if ( ! simple) {
                g = g.simple();
                assert(g.is_symmetric() && g.is_simple());
                g.stats();
            }
            vcdim vcg(g);
            std::cout << vcg.greedy_vcdim_lb(maxvisits)
                      << std::endl;
        } else if (com == "nb-shattered-visited-red") {
            std::ifstream ifs(args[i++]);
            check_open(ifs);
            graph g(ifs);
            ifs.close();
            bool simple = g.is_simple();
            std::cerr <<"is simple: "<< simple <<"\n";
            if ( ! simple) {
                g = g.simple();
                assert(g.is_symmetric() && g.is_simple());
                g.stats();
            }
            vcdim vcg1(g);
            uint d = vcg1.compute_vcdim(); // get right lower_bound
            vcdim vcg2(g, d);
            assert(d == vcg2.compute_vcdim(ordering(1),
                                           false, false,
                                           true,
                                           false, true));
            std::cout << vcg2.get_nb_shattered()
                      << std::endl;
        } else if (com == "nb-shattered-visited") {
            std::ifstream ifs(args[i++]);
            check_open(ifs);
            graph g(ifs);
            ifs.close();
            bool simple = g.is_simple();
            std::cerr <<"is simple: "<< simple <<"\n";
            if ( ! simple) {
                g = g.simple();
                assert(g.is_symmetric() && g.is_simple());
                g.stats();
            }
            vcdim vcg1(g);
            uint d = vcg1.compute_vcdim(); // get right lower_bound
            vcdim vcg2(g, d);
            assert(d == vcg2.compute_vcdim(ordering(1),
                                           false, false,
                                           false,
                                           false, true));
            std::cout << vcg2.get_nb_shattered()
                      << std::endl;
        } else if (com == "nb-shattered-highdeg") {
            std::ifstream ifs(args[i++]);
            check_open(ifs);
            graph g(ifs);
            ifs.close();
            bool simple = g.is_simple();
            std::cerr <<"is simple: "<< simple <<"\n";
            if ( ! simple) {
                g = g.simple();
                assert(g.is_symmetric() && g.is_simple());
                g.stats();
            }
            vcdim vcg1(g);
            uint d = vcg1.compute_vcdim(); // get right lower_bound
            vcdim vcg2(g, d);
            assert(d == vcg2.compute_nb_shattered(d));
            std::cout << vcg2.get_nb_shattered()
                      << std::endl;
        } else if (com == "nb-shattered-all") {
            std::ifstream ifs(args[i++]);
            check_open(ifs);
            graph g(ifs);
            ifs.close();
            bool simple = g.is_simple();
            std::cerr <<"is simple: "<< simple <<"\n";
            if ( ! simple) {
                g = g.simple();
                assert(g.is_symmetric() && g.is_simple());
                g.stats();
            }
            vcdim vcg(g);
            uint d = vcg.compute_nb_shattered(0);
            std::cerr <<"vcdim="<< d <<"\n";
            std::cout << vcg.get_nb_shattered()
                      << std::endl;
        } else if (com == "vcdim-lb-exact") {
            std::ifstream ifs(args[i++]);
            check_open(ifs);
            graph g(ifs);
            ifs.close();
            std::cout << vcdim_lower_bound(g, true)
                      << std::endl;
        } else if (com == "erdos-renyi") {
            uint n = uint(std::stoi(args[i++]));
            double p = std::stod(args[i++]);
            graph g = erdos_renyi_random_graph(n, p);
            std::cerr <<"is simple: "<< g.is_simple() <<"\n";
        } else if (com == "erdos-renyi-curve") {
            uint n = uint(std::stoi(args[i++]));
            double p_beg = std::stod(args[i++]);
            double dp = std::stod(args[i++]);
            double p_end = std::stod(args[i++]);
            uint nbavg = uint(std::stoi(args[i++]));
            std::string dummy(args[i++]);
            int seed = 123;
            for (double p = p_beg; p <= p_end+0.00001; p += dp) {
                std::cerr <<"p="<< p <<" "<< int(p / dp)
                          <<" / "<< int(1.0 / dp) <<"\n";
                double sum = 0., nb = 0., var = 0.;
                for (uint i = 0; i < nbavg; ++i) {
                    graph g = erdos_renyi_random_graph(n, p, seed++);
                    vcdim vcg(g);
                    uint d = vcg.compute_vcdim();
                    std::cerr << n <<" "<< i <<" "<< p <<" "<< d << std::endl;
                    sum += d;
                    var += d*d;
                    nb += 1.0;
                }
                double avg = sum / nb;
                double dev = std::sqrt(var / nb - avg * avg);
                std::cout << n <<" "<< p <<" "<< avg <<" "<< dev <<"\n";
            }
        } else if (com == "power-law") {
            uint n = uint(std::stoi(args[i++]));
            double beta = std::stod(args[i++]);
            graph g = power_law_random_graph(n, beta);
            std::cerr <<"is simple: "<< g.is_simple() <<"\n";
            std::cout <<"# "<< g.nb_nodes() <<" nodes "
                      << g.nb_edges() <<" edges\n";
            for (edge e : g.edges(true, true)) {
                std::cout << e.tail <<" "<< e.head << std::endl;
            }
        } else if (com == "power-law-curve") {
            uint n = uint(std::stoi(args[i++]));
            double beta_beg = std::stod(args[i++]);
            double db = std::stod(args[i++]);
            double beta_end = std::stod(args[i++]);
            uint nbavg = uint(std::stoi(args[i++]));
            std::string dummy(args[i++]);
            int seed = 123;
            for (double beta = beta_beg; beta <= beta_end+0.00001; beta += db) {
                std::cerr <<"beta="<< beta <<" "<< int(beta / db)
                          <<" / "<< 1+int(4.0 / db) <<"\n";
                double sum = 0., nb = 0., var = 0.;
                for (uint i = 0; i < nbavg; ++i) {
                    graph g = power_law_random_graph(n, beta, seed++);
                    vcdim vcg(g);
                    uint d = vcg.compute_vcdim();
                    std::cerr << n <<" "<< i <<" "<< beta <<" "<< d <<"\n";
                    sum += d;
                    var += d*d;
                    nb += 1.0;
                }
                double avg = sum / nb;
                double dev = std::sqrt(var / nb - avg * avg);
                std::cout << n <<" "<< beta <<" "<< avg <<" "<< dev <<"\n";
            }
        } else if (com == "shattered") {
            std::ifstream ifs(args[i++]);
            check_open(ifs);
            graph g(ifs);
            ifs.close();
            set_sets s{};
            while (i < args.size()) {
                s.push_back(std::stoi(args[i++]));
            }
            partition p(g.nb_nodes());
            bool shat = p.dual_shattered(g, s);
            std::cerr << s << (shat ? " is" : " is not") <<" shattered\n";
            std::cout << shat <<"\n";
        } else if (com == "degeneracy") {
            std::ifstream ifs(args[i++]);
            check_open(ifs);
            graph g(ifs);
            ifs.close();
            uint degen = g.degeneracy();            
            std::cerr <<"Degeneracy: "<< degen <<"\n";
            std::cout << degen <<"\n";
        } else if (com == "degeneracy-reduced-graph") {
            std::ifstream ifs(args[i++]);
            check_open(ifs);
            graph g(ifs);
            ifs.close();
            // Degeneracy
            uint degen = g.degeneracy();            
            std::cerr <<"Degeneracy: "<< degen <<"\n";
            // VC-dimension
            vcdim vcg(g);
            uint vcd = vcg.compute_vcdim();
            // Reduced graph
            uint plus_one = opt_uint_arg(0);
            vcg.reduce_graph(vcd + plus_one);
            degen = vcg.get_graph().degeneracy();
            std::cout << degen <<"\n";
        } else if (com == "sortgraph") {
            std::ifstream ifs(args[i++]);
            check_open(ifs);
            graph g(ifs);
            ifs.close();
            uint ntimes = i < args.size() ? std::stoi(args[i++]) : 10;
            while (ntimes-- > 0) {
                graph h = g.sort();
                if (ntimes == 0) h.stats();
            }
        } else if (com == "sortedges") {
            std::ifstream ifs(args[i++]);
            check_open(ifs);
            graph g(ifs);
            ifs.close();
            uint ntimes = i < args.size() ? std::stoi(args[i++]) : 10;
            while (ntimes-- > 0) {
                std::vector<edge> edg = g.edges(false, true);
                if (ntimes == 0) std::cerr << edg.size() <<" edges\n";
            }
        } else if (com == "kcore") {
            std::ifstream ifs(args[i++]);
            check_open(ifs);
            graph g(ifs);
            ifs.close();
            std::vector<uint> rank = g.kcore_ranking(true);
        } else {
            std::cerr << "\nUnkown command: " << com << std::endl;
            usage_exit(argv);
        }
    }

}

