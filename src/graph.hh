/** Undirected multigraph with vertices 0..n-1.
 * Implemented as a set system of closed neighborhoods, that is
 * each node is listed among its neighbors.
 */

#pragma once

#include <iostream>

#include "set_system.hh"

using node = elt;

struct edge {
    node tail, head;
    edge(node t, node h) : tail(t), head(h) {}
    bool operator==(const edge e) const {
        return tail == e.tail && head == e.head;
    }
};

class graph : public set_system {
public:

    graph() : set_system() {}
    graph(std::istream& is) ; // input a graph from 1 edge/line format
    uint nb_nodes() const { return nb_elts(); }
    uint nb_edges() const {
        return (sum_set_sizes() - nb_elts()) / 2;
        // remove self-loops, count each edge once
    }
    void add_node(node u) {
        add_set(u);
        add_elt(u);
        if (set_size(u) == 0) { add(u, u); } // u is in its closed neighborhood
    }
    void add_edge(node u, node v) {
        add_node(u);
        add_node(v);
        add(u, v);
        add(v, u);
    }
    uint_range nodes() const { return sets(); }
    const set_elts& neighbors(node u) const { return operator[](u); } // including u
    uint degree(node u) const {
        return set_size(u) - 1;// does not count u itself // This appeared to be a BAD IDEA afterwards!
    }

    graph sort() const ; // same graph with sorted adjacency lists
    std::vector<edge> edges(bool simple = true, bool sort = false) const ; // sorted list of edges, with simple = true, only edges u,v with u < v are output
    graph simple() const; // remove duplicate edges
    bool operator==(const graph & g) const ; // equality
    bool is_symmetric() const ; // check that the graph is symmetric
    bool is_simple() const ; // check that the graph is simple
    
    void dfs_ordering(std::vector<node>::iterator it, node u) const ;

    static void unit() ; // unit tests

    graph induced_subgraph(const std::vector<node> & set,
                           std::vector<node> & mapping) const ;

    std::vector<node> twin_classes() const ; // return one node per class of (true) twins

    void stats() const ;

    uint degeneracy() const ;

    std::vector<uint> kcore_ranking(bool verbose = false) const ; // ranks nodes according to their kcore number (smaller k gets smaller rank).
    std::vector<uint> kcore_ranking (const std::vector<uint> &prerank) const ; // same but preserves prerank ordering (nodes with same prerank get reranked among them).
    
private:
    std::vector<node>::iterator
    dfs_ordering(std::vector<node>::iterator it, node u,
                 std::vector<node> & visited) const ;


};

graph erdos_renyi_random_graph(uint n, float p, int seed=123);
graph power_law_random_graph(uint n, float beta, int seed=123,
                             bool no_self_loop=false);
