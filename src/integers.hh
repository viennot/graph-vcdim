/** Integers used and helper class for ranges of integers.
 */

#pragma once

#include<cstdint>

using uint = std::uint_least32_t;

uint log_2(uint n) ;


/** A range of uints, 
 * for example,
 for (uint i : uint_range(0, 10)) std::cout << i;
 * prints:
0123456789
 */

class uint_iterator { // iterator for uint_range
    uint i;
public:
    uint_iterator(uint i) : i(i) {}
    uint operator*() const { return i; }
    uint_iterator &operator++() { ++i; return *this; }
    uint_iterator &operator--() { --i; return *this; }
    bool operator!=(const uint_iterator& o) { return i != o.i; }
};

class uint_range { // a range of uints
public:
    using iterator = uint_iterator;
private:
    const iterator _beg, _end;
public:
    uint_range(uint beg, uint end) : _beg(beg), _end(end) {} // _beg to _end-1
    iterator begin() const { return _beg; }
    iterator end() const { return _end; }
};

