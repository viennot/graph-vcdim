#include "integers.hh"

uint log_2(uint n) {
    uint l = 0, p = 1;
    while (p << 1 <= n) { ++l; p <<= 1; }
    return l;
}

