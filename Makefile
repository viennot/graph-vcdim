build: _build
	cd $<; make

_build: CMakeLists.txt
	mkdir -p $@
	cd $@; cmake ..

unit: _build
	cd _build; make unit && ./unit


release:
	mkdir -p _build
	cd _build; cmake -DCMAKE_BUILD_TYPE:STRING=Release .. && make


clean:
	rm -f *~ src/*~
	rm -fr _build



example: build
	_build/main vcdim unit_data/BIOGRID-SYSTEM-Dosage_Rescue-3.4.145.bcc.edgelist



%.lb: build
	_build/main vcdim-lb $*

%.exact: build
	_build/main vcdim-lb-exact $*


graphs-lb: build
	for f in $$(wc ../hyperbolicity/graphs/* | sort -n | sed -e 's/[^.]*//'); do \
	    echo $$f ;\
	    _build/main vcdim-lb $$f ;\
	done

