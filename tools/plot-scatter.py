import sys
import numpy as np
import plotly.express as px

xfile = sys.argv[1]
yfile = sys.argv[2]
zfile = sys.argv[3]
outfile = sys.argv[4]

x = np.loadtxt(xfile, dtype=float, comments='#', delimiter=' ')
y = np.loadtxt(yfile, dtype=float, comments='#', delimiter=' ')
z = np.loadtxt(zfile, dtype=float, comments='#', delimiter=' ')

zer = np.zeros(len(z))

fig = px.scatter(x=x, y=y, size=np.maximum(zer, z))

fig.update_xaxes(title_text=xfile)
fig.update_yaxes(title_text=yfile)
fig.update_layout(
    width=1200,
    height=1200,
    margin=dict(l=0, r=0, t=0, b=0),
)

fig.show()

fig.write_image(outfile)
