import sys
import pandas as pd
import plotly.express as px
import plotly.io as pio   
pio.kaleido.scope.mathjax = None

# args: nbcols [col names] x y color
# data is read from sys.stdin

ncols = int(sys.argv[1])
x = sys.argv[2+ncols]
y = sys.argv[2+ncols+1]
c = sys.argv[2+ncols+2]
e = sys.argv[2+ncols+3]
outfile = sys.argv[2+ncols+4]

data = pd.read_csv(sys.stdin, sep=" ", header=None)
data.columns = sys.argv[2:2+ncols]


if e != '': fig = px.line(data, x=x, y=y, color=c, error_y=e, markers=True)
else : fig = px.line(data, x=x, y=y, color=c, markers=True)
#, line_shape='spline')

fig.update_layout(
    width=600,
    height=400,
    margin=dict(l=0, r=0, t=0, b=0),
)

#fig.show()

fig.write_image(outfile)


