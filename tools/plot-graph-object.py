import sys
#import plotly.express as px
import plotly.graph_objects as go
import numpy as np
import plotly.io as pio   
pio.kaleido.scope.mathjax = None

# xcol xcol_name ycol ycol_name error_col file
xcol = int(sys.argv[1])
xname = sys.argv[2]
ycol = int(sys.argv[3])
yname = sys.argv[4]
file = sys.argv[5]

def add(fig, f, xcol, xname, ycol, yname):
    data = np.loadtxt(f, dtype=float, comments='#', delimiter=' ')
    fig.add_trace(go.Scatter(x=data[:,xcol], y=data[:,ycol],
                             error_y=dict( type='data',
                                           array=data[:,3] ),
                             mode='lines+markers'))

fig = go.Figure()

# data = np.loadtxt('_res/none-6h_erdos-renyi-curve_256_0.05',
#                   dtype=float, comments='#', delimiter=' ')

# fig = px.line(data, x=1, y=2, #line_shape='spline',
#               width=600, height=400,
#               labels={'1':"p", '2':"VC-dimension"})

add(fig, file, xcol, xname, ycol, yname)

fig.update_xaxes(title_text=xname)
fig.update_yaxes(title_text=yname)
fig.update_layout(
    width=600,
    height=400,
    margin=dict(l=0, r=0, t=0, b=0),
)

#fig.show()

fig.write_image(file + ".pdf")


