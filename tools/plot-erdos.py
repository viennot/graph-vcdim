import sys
import math
import pandas as pd
import plotly.express as px
import plotly.io as pio
import time
#pio.kaleido.scope.mathjax = None

# args: nbcols [col names] x y color
# data is read from sys.stdin

ncols = int(sys.argv[1])
x = sys.argv[2+ncols]
y = sys.argv[2+ncols+1]
c = sys.argv[2+ncols+2]
e = sys.argv[2+ncols+3]
outfile = sys.argv[2+ncols+4]

data = pd.read_csv(sys.stdin, sep=" ", header=None)
data.columns = sys.argv[2:2+ncols]

labels={
    'p': "$p$",
    'size': "$\\mbox{#nodes } n$",
    'VC-dim': "VC-dimension"
}

if e != '': fig = px.line(data, x=x, y=y, color=c, error_y=e, markers=True)
else : fig = px.line(data, x=x, y=y, color=c, markers=True, labels=labels)
#, line_shape='spline')

#fig.add_hline(y=math.log2(100)-1, line_dash="solid", line_width=1)
#fig.add_shape(type="line", x0=vertical_date_time, y0=0.9, x1=vertical_date_time, y1=1.2, line_width=2, line_dash="dash", line_color="green")

# for n in [32, 100, 256] :
#     # vcdim >= 4 for p > n^{-11/20}
#     fig.add_vline(x=pow(n,-0.55), line_dash="dot", line_width=1,
#                   annotation_text="$p = %d ^ {-0.55}$" % n,
#                   #annotation_font=dict(size=10, color="black"),
#                   annotation_font=dict(size=9),
#                   annotation_textangle=-90,
#                   annotation_position="top left")
# i=0
# d=3
# for n in [] : #400, 256, 128, 100, 64, 45, 32] :
#     i = i + 1
#     fig.add_annotation(x=pow(n,-0.55), y=3,
#                        text="$p = %d ^ {-0.55}$" % n,
#                        font=dict(size=8),
#                        axref="x", ayref="y",
#                        ax=pow(n,-0.55)+0.07+0.04*i, ay=3-0.4-0.22*(8-i),
#                        showarrow=True,
#                        arrowhead=1)
# for n in [] :
#     # vcdim >= 4 for p > n^{-11/20}
#     fig.add_shape(type="line",
#                   x0=pow(n,-0.55), y0=2.5,
#                   x1=pow(n,-0.55), y1=3.5,
#                   line_dash="dot", line_width=1, line_color="green")
# fig.add_hline(y=3, line_dash="dot", line_width=1)
    
# for n in [128, 256, 400] :
#     # vcdim >= 5 for p > n^{-21/55}
#     fig.add_vline(x=pow(n,-0.381818182), line_dash="dashdot",
#                   line_width=1, line_color="gray",
#                   annotation_text="$p = %d ^ {-0.38}$" % n,
#                   annotation_xshift= 1 if n==256 or n==128 else 0,
#                   annotation_font=dict(size=9, color="gray"),
#                   annotation_textangle=-90,
#                   annotation_position="bottom left")
# fig.add_hline(y=4, line_dash="dashdot", line_width=1, line_color="gray")

# for n in [256,400] :
#     # vcdim >= 6 for p > n^{-7/24}
#     pos="top left"
#     #if n != 256 : pos="bottom left"
#     fig.add_vline(x=pow(n,-0.2916666667), line_dash="dash", line_width=1,
#                   annotation_text="$p = %d ^ {-0.29}$" % n,
#                   annotation_xshift= 1,
#                   annotation_font=dict(size=9),
#                   annotation_textangle=-90,
#                   annotation_position=pos)
# fig.add_hline(y=5, line_dash="dash", line_width=1)

fig.update_layout(
    #xaxis_range=[-0.01,0.3],
    width=600,
    height=400,
    margin=dict(l=0, r=0, t=0, b=0),
    legend={'traceorder':'reversed'}
)

#fig.show()

fig.write_image("/tmp/mathjax-loading-pb.pdf")
time.sleep(1)

fig.write_image(outfile)


