# Makefile to produce experiments.
#
# It mainly produces a bunch of targets from a list of inputs on a cluster using slurm.
#
# All input files f must be listed in $(INPS).
# Each target _res/f is produced by running $(EXEC) $(ARGS) f.
# A target is considered to be successfully produced if the command outputs anything.
#
# Example: make all.batch.ssh ARGS="-a 1" WORKER=4
#   will connect to the cluster and produce all targets in $(OUTS)
#   using 4 parallel (slurm) jobs.
#

help:
	@echo '`make all` will produce main targets, '
	@echo '`make compile; make exps` will do all experiments.'
	@echo '`export LC_NUMERIC=en; make alltables allfigs` \
		for tables and figures.' 
	@echo '`make world` will do everything.'

world:
	$(MAKE) data
	$(MAKE) compile
	+$(MAKE) exps
	export LC_NUMERIC=en; $(MAKE) alltables allfigs

data: data-raw
	cd data-raw; make sel
	ln -s data-raw/_simple data

data-raw:
	git clone https://gitlab.inria.fr/graph/data.git
	mv data data-raw

exps: \
	exps1 \
	exps2 \
	read.mkall \
	vcdim-lb-opt_16.mkall vcdim-lb-opt_32.mkall vcdim-lb-opt_64.mkall \
	vcdim-lb-opt_128.mkall vcdim-lb-opt_256.mkall vcdim.mkall \
	bounds \
	exprand \
	nb-shattered-visited-red.mkall nb-shattered-visited.mkall \
	nb-shattered-highdeg.mkall nb-shattered-all.mkall

expsmin: \
	exps1 \
	read.mkall \
	vcdim-lb-opt_16.mkall vcdim-lb-opt_32.mkall vcdim-lb-opt_64.mkall \
	vcdim-lb-opt_128.mkall vcdim-lb-opt_256.mkall \
	bounds \
	nb-shattered-visited-red.mkall nb-shattered-visited.mkall \
	nb-shattered-highdeg.mkall nb-shattered-all.mkall


exprand: erdos-renyi power-law 

exps1: \
	vcdimopt_5_0_0_1_0_1_64.mkall vcdimopt_2_0_0_1_0_1_64.mkall \
	vcdimopt_1_0_0_1_0_1_64.mkall vcdimopt_4_0_0_1_0_1_64.mkall \
	vcdim-overhead_1_0_0_1_0_1_64.mkall \
	vcdimopt_1_0_0_1_0_0_64.mkall vcdimopt_1_0_0_0_0_1_64.mkall \
	vcdimopt_1_0_0_0_0_0_64.mkall

exps2: \
	vcdimopt_1_0_0_0_0_1_32.mkall vcdimopt_2_0_0_0_0_1_32.mkall \
	vcdimopt_4_0_0_0_0_1_32.mkall \
	vcdimopt_1_0_0_1_0_1_32.mkall vcdimopt_2_0_0_1_0_1_32.mkall \
	vcdimopt_4_0_0_1_0_1_32.mkall 

bounds: vcdim-bounds.mkall


%.mkall:
	+$(MAKE) allmk ARGS="`echo $* | tr '_' ' '`" TIMEOUT=$(TIMEOUT)

alltables: _res/nshat.tex
	@echo; echo " -------- Table 1 -------"
	@$(MAKE) -s selres2 ARGS="vcdimopt 1 0 0 1 0 1 64" | tee _res/table1.tex
	@echo; echo " -------- Table 2 -------"
	@$(MAKE) -s lbres  | tee _res/table2.tex
	@echo; echo " -------- Table 3 -------"
	@$(MAKE) -s optres | tee _res/table3.tex
	$(MAKE) -s _res/bounds.tex BOLD=3
_res/nshat.tex:
	$(MAKE) nshatres > /dev/null
	$(MAKE) nshatres > $@

allfigs: \
	_res/nshat-compl.pdf _res/nshat-lem1.pdf \
	_res/nshat.pdf _res/nshat-hd.pdf \
	_res/erdos-renyi.pdf _res/erdos-renyi-zoom.pdf \
	_res/power-law.pdf


# ----------- Define the following variables according to your needs:

# Cluster and working directory on the cluster.
CLUST:=
CLUST_DIR:=graph/vcdim

# Files to copy to the cluster (include this makefile, named clust.mk a priori).
ALL_FILES:=clust.mk Makefile CMakeLists.txt src

# Executable and how to build it.
EXEC:=_build/main
COMPILE:=make
MODULES:=module load gnu8 cmake

# The list of all inputs:
INPS:=$(wildcard data/*/*.edgelist data/*/*.txt) \
	$(patsubst %.orig,%.txt,$(wildcard data/*/*.orig)) \
	$(patsubst %.txt.gz,%.txt,$(wildcard data/*/*.txt.gz))
# selection:
#SELS:=$(shell echo $(INPS) | sed -e 's|[^ ]*/||g')
SELS:=BIOGRID-MV-Physical-3.5 BIOGRID-SYSTEM-Affinity_Capture-MS-3.5 BIOGRID-SYSTEM-Affinity_Capture-RNA-3.5 dip20170205 \
  --sep-- \
oregon2_010331 CAIDA_as_20130601 DIMES_201204 as-skitter \
  --sep-- \
p2p-Gnutella09 gnutella31 notreDame y-BerkStan \
  --sep-- \
ca-HepPh com-dblp epinions1 facebook_combined twitter_combined \
  --sep-- \
t.CAL t.FLA buddha froz z-alue7065 \
  --sep-- \
grid300-10 xgrid500-10 powerlaw2.5
INPS:=$(shell echo $(INPS) | tr " " "\012" | grep -E "(`echo $(SELS) | tr " " "|"`)")

SEL_ARGS:=vcdimopt 1 0 0 1 0 1 64

# Default timeout and args:
WALLTIME:=96:00:00
TIMEOUT:=$(if $(TIMEOUT),$(TIMEOUT),6h)
ARGS:=$(if $(ARGS),$(ARGS),)


# rm data/sources_graphs/froz-w.txt data/sources_graphs/email-Enron.txt data/sources_graphs/epinions1-d.txt data/sources_graphs/facebook_combined.txt data/massive/skitter.txt

# mon_mac: _build/main vcdimopt 1 1 0 1 data/massive/skitter.txt  4738.54s user 38.82s system 95% cpu 1:22:59.94 total

# ----------- The ramining variables should work fine:

DIR_INP:=data
# DIR_OUT:=_res
OUTS:=$(subst $(DIR_INP)/,_res/,$(INPS))

SHELL := /bin/bash
RSYNC:=rsync -v -e ssh --copy-unsafe-links --inplace -r --relative --exclude '*~' --exclude '\#*' --exclude '_*'
ME:=$(shell whoami)
MAKE:=make -f clust.mk

_scp:  $(ALL_FILES)
	$(RSYNC) $(ALL_FILES) $(CLUST):$(CLUST_DIR)/
	touch $@

push:
	@if grep REM src/*.hh src/*.cc ; then \
		echo "Something to remove?" 1>&2; \
		exit 3; \
	fi
	@if [ -z "$(MSG)" ]; then \
		echo "MSG undefined!" 1>&2; \
		exit 4; \
	fi
	git commit -m '$(MSG)' -a
	git push

# -------------------- compile ------------------------

#SUFFIX:=$(shell echo "$(TIMEOUT) $(ARGS)" | tr "/ " "-_")
SUFFIX:=$(shell echo "$(ARGS)" | tr "/ " "-_")

OUTS_SUFF:=$(patsubst %,%-$(TIMEOUT)_$(SUFFIX),$(OUTS))

.SUFFIXES:


compile:
	$(COMPILE)

info:
	@echo SELS: $(SELS)
	@echo INPS: $(INPS) | tr " " "\012" | sort
	@echo OUTS: $(OUTS)
	@echo "|SELS| = `echo $(SELS) | tr " " "\012" | grep -v -e --sep--| wc -l`"
	@echo "|OUTS| = `echo $(OUTS) | wc -w`"
	@echo OUTS[1]: $(word 1,$(OUTS))
	@echo EXEC: $(EXEC)
	@echo ARGS: $(ARGS)
	@echo TIMEOUT: $(TIMEOUT)
	@echo SUFFIX: "$(SUFFIX)"
	$(eval PAT:=$(if $(PAT),$(PAT),skit))
	@echo "OUTgrep PAT=$(PAT): "; \
		echo $(OUTS) | tr " " "\012" | grep -e $(PAT)

test/%$(SUFFIX):
	$(eval V:="var")
	echo this #com \
		and that
	@echo test: $@ $* $(SUFFIX) '$(V)'

all: compile
	for f in $(OUTS); do \
		echo $$f-$(TIMEOUT)_$(SUFFIX); \
		$(MAKE) $$f-$(TIMEOUT)_$(SUFFIX) ARGS="$(ARGS)" TIMEOUT=$(TIMEOUT); \
	done

allmk: $(OUTS_SUFF)

none: _data/none/n
	+$(MAKE) _res/none/n-$(TIMEOUT)_$(SUFFIX) \
			ARGS="$(ARGS)" TIMEOUT=$(TIMEOUT)
_data/none/n:
	mkdir -p _data/none
	touch _data/none/n

%.mknone:
	+$(MAKE) none ARGS="`echo $* | tr '_' ' '`" TIMEOUT=$(TIMEOUT)

epinions: compile _res/more/massive/epinions1-d.bcc.txt-$(TIMEOUT)_$(SUFFIX)

twitter: _res2/massive/twitter_combined-d.txt-5h_vcdimopt_1_0_0_1_0_1_64

erdos-renyi: \
    erdos-renyi-curve_32_0._0.02_1.0_20.mknone \
    erdos-renyi-curve_45_0._0.02_1.0_20.mknone \
    erdos-renyi-curve_64_0._0.02_1.0_20.mknone \
    erdos-renyi-curve_100_0._0.02_1.0_20.mknone \
    erdos-renyi-curve_128_0._0.02_1.0_20.mknone \
    erdos-renyi-curve_256_0._0.02_0.4_20.mknone \
    erdos-renyi-curve_400_0._0.02_0.3_20.mknone

power-law: \
    power-law-curve_32_0.5_0.1_4.5_20.mknone \
    power-law-curve_64_0.5_0.1_4.5_20.mknone \
    power-law-curve_128_0.5_0.1_4.5_20.mknone \
    power-law-curve_256_0.5_0.1_4.5_20.mknone \
    power-law-curve_512_0.5_0.1_4.5_20.mknone \
    power-law-curve_1000_1.2_0.1_4.5_20.mknone \
    power-law-curve_2000_1.4_0.1_4.5_20.mknone \
    power-law-curve_4000_1.6_0.1_4.5_20.mknone \
    power-law-curve_30000_2.0_0.1_4.5_20.mknone \
    power-law-curve_300000_2.2_0.1_4.5_20.mknone \
    power-law-curve_1000000_2.3_0.1_4.5_20.mknone

_res/erdos-renyi.pdf: tools/plot-erdos.py
	cat `ls _res/none/*erdos*0 | grep -v -e _400 -e _256` \
	| sort -n -k 1 \
	| python3 $< 4 size p VC-dim dev p VC-dim size '' $@

_res/erdos-renyi-zoom.pdf: tools/plot-erdos-zoom.py
	cat _res/none/*erdos*0 \
	| sort -n -k 1 \
	| python3 $< 4 size p VC-dim dev p VC-dim size '' $@

_res/power-law.pdf: tools/plot-power.py
	cat _res/none/*power*0 \
	| sort -n -k 1 \
	| python3 $< 4 size beta VC-dim dev beta VC-dim size '' $@


_data/%.txt: _data/%.orig
	@mkdir -p /tmp/$(ME)
	$(eval TMP:=$(shell mktemp -u /tmp/$(ME)/_data.XXXX))
	$(EXEC) cleangraph $< > $@

.PRECIOUS: _data/%

_data/%: $(DIR_INP)/%
	@mkdir -p `dirname $@`
	cat $< | sed -e 's/^a //' -e 's/|/ /g' | tr "\011" " " | grep -v -e '90594 325070 0 0 1 end' > $@


LSCPU:=lscpu
LSMEM:=$(shell if which lsmem > /dev/null ; \
	 then echo lsmem; \
	 else echo "sysctl -a"; fi)
BINTIME:=$(shell if /usr/bin/time -v ls > /dev/null 2>&1; \
	then echo "/usr/bin/time -v"; \
	else echo "/usr/bin/time -l"; fi)

infoenv:
	echo $(LSCPU) $(LSMEM) $(BINTIME)

_res/%-$(TIMEOUT)_$(SUFFIX): _data/%
	@mkdir -p `dirname $@`
	@mkdir -p /tmp/$(ME)
	$(eval TMP:=$(shell mktemp -u /tmp/$(ME)/_res.XXXX))
	@if [ -z "`cat _res/$*-*_$(SUFFIX) 2>/dev/null | head -n 1`" \
	     -a "`ls _res/$*-*_$(SUFFIX) 2>/dev/null | wc -l`" -eq "`cat _res/$*-*_$(SUFFIX).err 2>/dev/null | grep 'exited with non-zero status 124' | wc -l`" \
	   ]; then \
		echo "go: $@"; \
		touch $@; \
		($(LSCPU) && echo '======' && $(LSMEM) && echo '======' \
		 && echo 'run "$(EXEC) $(ARGS) $<" for $(TIMEOUT)' \
		 && echo '======' \
		 && date +"%Y-%m-%d:%Hh%M" | tr "\012" " " && date) > $@.env; \
		 $(BINTIME) timeout $(TIMEOUT) $(EXEC) $(ARGS) $< \
			> $(TMP) 2> $@.err && mv -f $(TMP) $@; \
	else \
		echo "skip: $@"; \
		echo "  done (other timeout), or in progress, or in error:";\
		echo "  `ls _res/$*-*_$(SUFFIX)`"; \
		touch $@; \
	fi


_res2/%-$(TIMEOUT)_$(SUFFIX): _data/%
	@mkdir -p `dirname $@`
	@mkdir -p /tmp/$(ME)
	$(eval TMP:=$(shell mktemp -u /tmp/$(ME)/_res2.XXXX))
	@if [ -z "`cat _res2/$*-*_$(SUFFIX) 2>/dev/null | head -n 1`" \
	     -a "`ls _res2/$*-*_$(SUFFIX) 2>/dev/null | wc -l`" -eq "`cat _res2/$*-*_$(SUFFIX).err 2>/dev/null | grep 'exited with non-zero status 124' | wc -l`" \
	   ]; then \
		echo "go: $@"; \
		touch $@; \
		($(LSCPU) && echo '======' && $(LSMEM) && echo '======' \
		 && echo 'run "$(EXEC) $(ARGS) $<" for $(TIMEOUT)' \
		 && echo '======' \
		 && date +"%Y-%m-%d:%Hh%M" | tr "\012" " " && date) > $@.env; \
		 $(BINTIME) timeout $(TIMEOUT) $(EXEC) $(ARGS) $< \
			> $(TMP) 2> $@.err && mv -f $(TMP) $@; \
	else \
		echo "skip: $@"; \
		echo "  done (other timeout), or in progress, or in error:";\
		echo "  `ls _res2/$*-*_$(SUFFIX)`"; \
		touch $@; \
	fi




res:
	$(eval PAT:=-*_$(SUFFIX))
	@mkdir -p /tmp/$(ME)
	$(eval TMP:=$(shell mktemp -u /tmp/$(ME)/_res.XXXX))
	(for fbase in $(OUTS); do \
		f=`ls -t $$fbase$(PAT).err 2>/dev/null | head -n 1 | sed -e 's/.err$$//'`; \
		status=9_unknown; \
		if [ ! -f "$$f" ]; \
		  then f=$$fbase; status=0_todo; \
		elif [ -s $$f ]; \
		  then status=1_done; \
		elif grep 'exited with non-zero status 124' $$f.err >/dev/null;\
		  then status=2_timeout; \
		elif grep 'exited with non-zero status' $$f.err >/dev/null; \
		  then status=4_error; \
		elif grep 'terminated by signal' $$f.err >/dev/null; \
		  then status=4_error; \
		elif grep 'User time' $$f.err >/dev/null; \
		  then status=4_no_out_or_error; \
		else \
		  status=3_running; \
		fi; \
		/bin/echo -n "$$status $$f "; \
		if [ -f $$f ]; then \
		  /bin/echo -n "date: "; \
		  tail -n 1 $$f.env 2>/dev/null| cut -d' ' -f1 | tr "\012" " ";\
		  /bin/echo -n "time: "; \
		  grep 'User time' $$f.err 2>/dev/null | tr "\011" " " \
		    | sed -e 's/ *User[^:]*: *//' | tr "\012" " "; \
		  /bin/echo -n "out: "; \
		  head -n 1 $$f 2>/dev/null | tr " \012" "_ "; \
		  /bin/echo -n "err: "; \
		  head -n 1 $$f.err 2>/dev/null | tr " \012" "_ "; \
		fi; \
		/bin/echo ""; \
	done) | tee $(TMP) | grep -v -e 0_todo -e 1_done  || echo all_done
	@cat $(TMP) | sort -n -k 6 | grep -e 1_done || :
	@ndone=`(grep 1_done $(TMP) || :) | wc -l`; n=`cat $(TMP) | wc -l`; \
	  echo "  ----- $$ndone / $$n done   \
	        sum_times=`$(MAKE) -s $(TMP).sum COL=6`"
	@cat $(TMP) | sort -k 4 | sort -s -k 1 | grep -v -e 0_todo -e 1_done|| :
	@rm -f $(TMP)

resm:
	$(eval PAT:=-*_$(SUFFIX))
	@mkdir -p /tmp/$(ME)
	$(eval TMP:=$(shell mktemp -u /tmp/$(ME)/_res.XXXX))
	(for fbase in $(OUTS); do \
		f=`ls -t $$fbase$(PAT).err 2>/dev/null | head -n 1 | sed -e 's/.err$$//'`; \
		status=9_unknown; \
		if [ ! -f "$$f" ]; \
		  then f=$$fbase; status=0_todo; \
		elif [ -s $$f ]; \
		  then status=1_done; \
		elif grep 'exited with non-zero status 124' $$f.err >/dev/null;\
		  then status=2_timeout; \
		elif grep 'exited with non-zero status' $$f.err >/dev/null; \
		  then status=4_error; \
		elif grep 'terminated by signal' $$f.err >/dev/null; \
		  then status=4_error; \
		elif grep 'User time' $$f.err >/dev/null; \
		  then status=4_no_out_or_error; \
		else \
		  status=3_running; \
		fi; \
		/bin/echo -n "$$status $$f "; \
		if [ -f $$f ]; then \
		  /bin/echo -n "date: "; \
		  tail -n 1 $$f.env 2>/dev/null| cut -d' ' -f1 | tr "\012" " ";\
		  /bin/echo -n "time: "; \
		  grep 'User time' $$f.err 2>/dev/null | tr "\011" " " \
		    | sed -e 's/ *User[^:]*: *//' | tr "\012" " "; \
		  /bin/echo -n "out: "; \
		  head -n 1 $$f 2>/dev/null | tr " \012" "_ "; \
		  /bin/echo -n "err: "; \
	if grep "^is simple: 0" $$f.err >/dev/null 2>&1; then \
		  grep 'm=' $$f.err 2>/dev/null \
		  | head -n 2 | tail -n 1 | tr " \012" "_ "; \
	else \
		  head -n 1 $$f.err 2>/dev/null | tr " \012" "_ "; \
	fi; \
		fi; \
		/bin/echo ""; \
	done) | tee $(TMP) | grep -v -e 0_todo -e 1_done  || echo all_done
	@cat $(TMP) | sort -n -k 6 | grep -e 1_done || :
	@ndone=`(grep 1_done $(TMP) || :) | wc -l`; n=`cat $(TMP) | wc -l`; \
	  echo "  ----- $$ndone / $$n done   \
	        sum_times=`$(MAKE) -s $(TMP).sum COL=6`"
	@cat $(TMP) | sort -k 4 | sort -s -k 1 | grep -v -e 0_todo -e 1_done|| :
	@rm -f $(TMP)

rawres:
	@$(MAKE) -s res ARGS="$(ARGS)" | sed -e 's|.*/||' -e 's/-6h_vc.* time:/ time:/' -e 's/_m/ m/' -e 's/$$/\\\\/' -e 's/_/-/g' 


SHORTNAME:=sed -e 's/_/-/g' -e 's/SYSTEM/SYS/' -e 's/Affinity.Capture/Aff-Cap/' -e 's/BIOGRID/BIO/' -e 's/-d$$//' -e 's/-w$$//' | tr -d "\012"

selres:
	@mkdir -p /tmp/$(ME)
	$(eval TMP:=$(shell mktemp -u /tmp/$(ME)/_res.XXXX))
	@$(MAKE) -s resm ARGS="$(if $(ARGS),$(ARGS),$(SEL_ARGS))" \
	  | sed -e 's|.*/||' -e 's/-[^-_]*_vc.* time:/ time:/' \
	  | sed -e 's/_m/ m/' > $(TMP)
	@for f in $(SELS); do \
	    if [ "$$f" = "--sep--" ]; then \
	        echo "\hline"; \
	    else \
		/bin/echo -n $$f | $(SHORTNAME); \
		cat $(TMP) | grep -E $$f | sed -e 's/^[^ ]* / /' \
		| sed -e 's/time: //' -e 's/out: //' -e 's/err: //' \
  -e 's/n=//' -e 's/m=//' -e 's/_isol.*=/ /' | awk '{ \
    n = $$3; m = $$4; if (NF > 4 && 0) n = $$3 - $$5; vcdim = $$2; time = $$1; \
    print " &", n, "&", m, "&", vcdim, "&", time, "\\\\" \
  }'; \
	    fi; \
	done
	@rm -f $(TMP)

optres:
	@mkdir -p /tmp/$(ME)
	@for f in $(SELS); do \
	    if [ "$$f" = "--sep--" ]; then \
	        echo "\hline"; \
	    else \
		/bin/echo -n "$$f " | $(SHORTNAME); \
		(for args in vcdimopt_5_0_0_1_0_1_64 vcdimopt_2_0_0_1_0_1_64 \
			     vcdimopt_1_0_0_1_0_1_64 vcdimopt_4_0_0_1_0_1_64 \
	    		     vcdimopt_1_0_0_1_0_0_64 vcdimopt_1_0_0_0_0_1_64 \
			     vcdimopt_1_0_0_0_0_0_64; do \
		    res=/tmp/$(ME)/$$args; \
		    if [ ! -f $$res ]; then \
			$(MAKE) -s res ARGS=$$args > $$res; \
		    fi; \
		    cat $$res | grep -E $$f | sort -u \
			      | sed -e 's/^3_running .*/21600/' \
			      | sed -e 's/^.*time: //' -e 's/ *out: .*//' \
			      | tr "\012" " "; \
	    	done) | awk '{ \
	min=$$1; \
	for (i = 1; i <= NF; i++) { if ($$(i) < min) min = $$(i); } \
	for (i = 1; i <= NF; i++) { \
		t=$$(i); \
		if (t > 21500) printf " & --"; \
		else if (t == min) printf " & \\textbf{%.2f}", t; \
		else printf " & %.2f", t; \
	} \
	print "\\\\"; \
  }'; \
	    fi; \
	done
	@rm -f /tmp/$(ME)/vcdim*

optres32:
	@mkdir -p /tmp/$(ME)
	@for f in $(SELS); do \
	    if [ "$$f" = "--sep--" ]; then \
	        echo "\hline"; \
	    else \
		/bin/echo -n "$$f " | $(SHORTNAME); \
		(for args in vcdimopt_5_0_0_1_0_1_64 vcdimopt_2_0_0_1_0_1_64 \
			     vcdimopt_1_0_0_1_0_1_64 vcdimopt_4_0_0_1_0_1_64 \
			     vcdimopt_1_0_0_1_0_1_32 vcdimopt_4_0_0_1_0_1_32 \
	    		     vcdimopt_1_0_0_1_0_0_64 vcdimopt_1_0_0_0_0_1_64 \
			     vcdimopt_1_0_0_0_0_0_64; do \
		    res=/tmp/$(ME)/$$args; \
		    if [ ! -f $$res ]; then \
			$(MAKE) -s res ARGS=$$args > $$res; \
		    fi; \
		    cat $$res | grep -E $$f | sort -u \
			      | sed -e 's/^3_running .*/21600/' \
			      | sed -e 's/^.*time: //' -e 's/ *out: .*//' \
			      | tr "\012" " "; \
	    	done) | awk '{ \
	min=$$1; \
	for (i = 1; i <= NF; i++) { if ($$(i) < min) min = $$(i); } \
	for (i = 1; i <= NF; i++) { \
		t=$$(i); \
		if (t > 21500) printf " & --"; \
		else if (t == min) printf " & \\textbf{%.2f}", t; \
		else printf " & %.2f", t; \
	} \
	print "\\\\"; \
  }'; \
	    fi; \
	done
	@rm -f /tmp/$(ME)/vcdim*

lbres:
	@mkdir -p /tmp/$(ME)
	@for f in $(SELS); do \
	    if [ "$$f" = "--sep--" ]; then \
	        echo "\hline"; \
	    else \
		/bin/echo -n "$$f" | $(SHORTNAME); \
		(for args in vcdimopt_1_0_0_1_0_1_64 \
			     vcdim-lb-opt_16  vcdim-lb-opt_32 vcdim-lb-opt_64 \
	    		     vcdim-lb-opt_128 vcdim-lb-opt_256; do \
		    res=/tmp/$(ME)/$$args; \
		    if [ ! -f $$res ]; then \
			$(MAKE) -s res ARGS=$$args > $$res; \
		    fi; \
		    cat $$res | grep -E $$f | sort -u \
			      | sed -e 's/^.*out: //' -e 's/ err: .*//' \
			      | tr "\012" " "; \
	    	done) | awk '{ \
	max=$$1; \
	for (i = 1; i <= NF; i++) { if ($$(i) > max) max = $$(i); } \
	for (i = 1; i <= NF; i++) { \
		lb=$$(i); \
		if (lb == max) printf " & \\textbf{%d}", lb; \
		else printf " & %d", lb; \
	} \
  }'; \
		for args in read \
			     vcdim-lb-opt_16  vcdim-lb-opt_32 vcdim-lb-opt_64 \
	    		     vcdim-lb-opt_128 vcdim-lb-opt_256; do \
		    res=/tmp/$(ME)/$$args; \
		    if [ ! -f $$res ]; then \
			$(MAKE) -s res ARGS=$$args > $$res; \
		    fi; \
		    /bin/echo -n " & " ; \
		    cat $$res | grep -E $$f \
			      | sed -e 's/^.*time: //' -e 's/ out: .*//' \
			      | tr -d "\012"; \
	    	done; \
		echo " \\\\"; \
	    fi; \
	done
	@rm -f /tmp/$(ME)/vcdim* /tmp/$(ME)/read*

_res/bounds.csv: _res/aggr/vcdim-bounds-time
	$(eval COLS:=vcdim-lb-opt_64 vcdimopt_1_0_0_1_0_1_64 \
		     vcdim-bounds vcdim-bounds-time)
	echo 'graph,lb64,VCdim,$$\floor{\log\Delta}+1$$,$$\floor{\log{n}}$$,degen+1' > $@;
	for s in $(SELS); do \
	    if [ "$$s" = "--sep--" ]; then \
		echo >> $@; \
	    else \
		: /bin/echo "$$s" | $(SHORTNAME); \
		/bin/echo -n "$$s" | $(SHORTNAME) >> $@; \
		fbase=$$(echo $(OUTS) | tr " " "\012" | grep -e $$s|head -n 1);\
		(for suffix in $(COLS); do \
		    val=$$(cat $${fbase}-*_$${suffix} 2>/dev/null | tail -n 1);\
		    /bin/echo -n " $$val"; \
		done) | tee /tmp/l | awk '{ \
		    t[0]=0; split("1 2 5 6 7", t, " "); \
		    for (i = 1; i <= NF - 3; i++) { \
			printf ",%s", $$(t[i]); \
	 	    } \
		}' >> $@; \
		echo >> $@; \
	    fi; \
	done

_res/bounds-loc.csv: _res/aggr/vcdim-bounds-time
	$(eval COLS:=vcdim-lb-opt_64 vcdimopt_1_0_0_1_0_1_64 \
		     vcdim-bounds vcdim-bounds-time)
	echo "graph,lb64,loc-lb,VCdim,loc-ub,deg-ub,n-ub,degen+1,time" > $@;
	for s in $(SELS); do \
	    if [ "$$s" = "--sep--" ]; then \
		echo >> $@; \
	    else \
		: /bin/echo "$$s" | $(SHORTNAME); \
		/bin/echo -n "$$s" | $(SHORTNAME) >> $@; \
		fbase=$$(echo $(OUTS) | tr " " "\012" | grep -e $$s|head -n 1);\
		(for suffix in $(COLS); do \
		    val=$$(cat $${fbase}-*_$${suffix} 2>/dev/null | tail -n 1);\
		    /bin/echo -n " $$val"; \
		done) | tee /tmp/l | awk '{ \
		    t[0]=0; split("1 3 2 4 5 6 7 8", t, " "); \
		    for (i = 1; i <= NF; i++) { \
			printf ",%s", $$(t[i]); \
	 	    } \
		}' >> $@; \
		echo >> $@; \
	    fi; \
	done

_res/%.tex: _res/%.csv
	cat $< | tr "," " " \
	| awk 'BEGIN{ print "\\hline" } { \
	    for (i = 1; i <= NF; i++) { \
		if (i > 1) printf " & "; \
		if (i == $(BOLD) || NR == 1) { \
			printf "\\textbf{%s}", $$(i); \
		} else { \
			printf "%s", $$(i); \
		} \
	    } \
	    if (NF > 0) print "\\\\"; \
	    if (NR == 1 || NF == 0) print "\\hline" ; \
	} END{ print "\\hline" }' > $@

degmax: _res/aggr/vcdim-bounds-time
	$(eval COLS:=vcdim-lb-opt_64 vcdim-bounds vcdim-bounds-time)
	echo "graph,lb64,loc-lb,deg-lb,n-lb,degen+1,time" > $@;
	for s in $(SELS); do \
	    if [ "$$s" = "--sep--" ]; then \
		:; \
	    else \
		: /bin/echo "$$s" | $(SHORTNAME); \
		/bin/echo -n "$$s" | $(SHORTNAME) >> $@; \
		fbase=$$(echo $(OUTS) | tr " " "\012" | grep -e $$s|head -n 1);\
		for suffix in $(COLS); do \
		    val=$$(cat $${fbase}-*_$${suffix} 2>/dev/null \
				| tail -n 1 | tr " " ",");\
		    /bin/echo -n ",$$val" >> $@; \
		done; \
		echo "" >> $@; \
	    fi; \
	done


hdegres:
	@for s in $(SELS); do \
	    if [ "$$s" = "--sep--" ]; then \
	        echo "\hline"; \
	    else \
		/bin/echo -n "$$s " | $(SHORTNAME); \
		fbase=$$(echo $(OUTS) | tr " " "\012" | grep -e $$s); \
		f=`ls -t $${fbase}-*_$(SUFFIX).err 2>/dev/null \
		    | head -n 1 | sed -e 's/.err$$//'`; \
		/bin/echo -n " & "; \
		head -n 1 $$fbase-*_vcdim-lb-opt_64 2>/dev/null \
		    | tr "\012" " "; \
		/bin/echo -n " & "; \
		head -n 1 $$f 2>/dev/null | tr "\012" " "; \
		/bin/echo -n " & "; \
		grep 'nodes with degree >=' $$f.err 2>/dev/null \
		    | tail -n 1 | sed -e 's/ nodes.*//' | tr "\012" " "; \
		/bin/echo -n " & "; \
		grep 'User time' $$f.err 2>/dev/null | tr "\011" " " \
		    | sed -e 's/ *User[^:]*: *//' | tr "\012" " "; \
		echo "\\\\"; \
	    fi; \
	done


nshatres: _res/aggr/nb-shattered-visited-red-highdeg \
		_res/aggr_eq_err/vcdimopt_1_0_0_1_0_1_64__avg_ball_size
	@mkdir -p /tmp/$(ME)
	@for f in $(SELS); do \
	    if [ "$$f" = "--sep--" ]; then \
	        echo "\hline"; \
	    else \
		/bin/echo -n "$$f" | $(SHORTNAME); \
		fbase=$$(echo $(OUTS) | tr " " "\012" \
		    | grep -e $$f | head -n 1); \
		for args in vcdimopt_1_0_0_1_0_1_64; do \
		    file=`ls -t $${fbase}-*_$${args}.err 2>/dev/null \
		        | head -n 1 | sed -e 's/.err$$//'`; \
		    vcdim=$$(tail -n 1 $$file); \
		    /bin/echo -n " & $$vcdim" ; \
	    	done; \
		for args in nb-shattered-all; do \
		    file=`ls -t $${fbase}-*_$${args}.err 2>/dev/null \
		        | head -n 1 | sed -e 's/.err$$//'`; \
		    nb=$$(grep 'nb_shattered=' $$file.err 2>/dev/null \
		        | tail -n 1 | sed -e 's/.*= *//' | tr "\012" " "); \
		    if [ "$$nb" == "" ]; then /bin/echo -n " & -- "; \
		    else /bin/echo -n " & $$nb "; fi; \
		done; \
		for args in  nb-shattered-highdeg \
			     nb-shattered-visited nb-shattered-visited-red \
			     nb-shattered-visited-red-highdeg; do \
		    res=/tmp/$(ME)/$$args; \
		    if [ ! -f $$res ]; then \
			$(MAKE) -s res ARGS=$$args > $$res; \
		    fi; \
		    nb=$$(cat $$res | grep -E $$f | sort -u \
			      | sed -e 's/^.*out: //' -e 's/ *err: .*//' \
			      | tr "\012" " "); \
		    if [ "$$nb" == " " ]; then /bin/echo -n " & -- "; \
		    else /bin/echo -n " & $$nb "; fi; \
	    	done; \
		for args in vcdimopt_1_0_0_1_0_1_64; do \
		    file=`ls -t $${fbase}-*_$${args}.err 2>/dev/null \
		        | head -n 1 | sed -e 's/.err$$//'`; \
		    /bin/echo -n " & " ; \
		    nb=$$(grep 'nb_shattered=' $$file.err 2>/dev/null \
		        | tail -n 1 | sed -e 's/.*= *//' | tr "\012" " "); \
		    if [ "$$nb" == "" ]; then nb="0"; fi; \
		    /bin/echo -n "$$nb & " ; \
		    grep 'nodes with degree >=' $$file.err 2>/dev/null \
			| tail -n 1 | sed -e 's/ nodes.*//' | tr "\012" " "; \
	    	done; \
		for args in vcdimopt_1_0_0_1_0_1_64-avg_ball_size; do \
		    res=/tmp/$(ME)/$$args; \
		    if [ ! -f $$res ]; then \
			$(MAKE) -s res ARGS=$$args > $$res; \
		    fi; \
		    nb=$$(cat $$res | grep -E $$f | sort -u \
			      | sed -e 's/^.*out: //' -e 's/ *err: .*//' \
			      | tr "\012" " "); \
		    if [ "$$nb" == " " ]; then /bin/echo -n " & 0 "; \
		    else /bin/echo -n " & `echo $$nb/1 | bc` "; fi; \
	    	done; \
		echo " \\\\"; \
	    fi; \
	done
	@rm -f /tmp/$(ME)/nb-shat* /tmp/$(ME)/vcdimopt*


_res/aggr/%-time:
	@mkdir -p `dirname $@`
	$(eval SUFFIX:=$*)
	for s in $(SELS); do \
	    if [ "$$s" = "--sep--" ]; then \
		:; \
	    else \
		fbase=$$(echo $(OUTS) | tr " " "\012" | grep -e $$s); \
		f=`ls -t $${fbase}-*_$(SUFFIX).err 2>/dev/null \
		    | head -n 1 | sed -e 's/.err$$//'`; \
		grep -e 'User time' $$f.err 2>/dev/null | tr "\011" " " \
		    | sed -e 's/ *User[^:]*: *//' \
		    | awk '{ if($$1 > 21500) printf "--\n"; \
			     else printf "%.2f\n", $$1; }' > $$f-time; \
		echo $$f-time; \
	    fi; \
	done;
	touch $@

_res/aggr/%-highdeg:
	@mkdir -p `dirname $@`
	$(eval SUFFIX:=$*)
	for s in $(SELS); do \
	    if [ "$$s" = "--sep--" ]; then \
		:; \
	    else \
		fbase=$$(echo $(OUTS) | tr " " "\012" | grep -e $$s); \
		echo "$$s $${fbase}-*_$(SUFFIX).err"; \
		f=`ls -t $${fbase}-*_$(SUFFIX).err 2>/dev/null \
		    | head -n 1 | sed -e 's/.err$$//'`; \
		grep 'nodes with degree >=' $$f.err 2>/dev/null \
			| tail -n 1 | sed -e 's/ nodes.*//' > $$f-highdeg; \
		touch $$f-highdeg.err; \
		echo $$f-highdeg; \
	    fi; \
	done;
	touch $@

_res/aggr_eq/%:
	@mkdir -p `dirname $@`
	$(eval SUFFIX:=$(call uucut,1))
	$(eval PATTERN:=$(call uucut,2))
	$(eval SP_PAT:=$(shell echo $(PATTERN) | tr "_" " "))
	for s in $(SELS); do \
	    if [ "$$s" = "--sep--" ]; then \
		:; \
	    else \
		fbase=$$(echo $(OUTS) | tr " " "\012" | grep -e $$s); \
		f=`ls -t $${fbase}-*_$(SUFFIX).err 2>/dev/null \
		    | head -n 1 | sed -e 's/.err$$//'`; \
		echo 's/.*$(SP_PAT) *[=:] *//'; \
		grep '$(SP_PAT)[^=:]*[=:]' $$f.err 2>/dev/null | tail -n 1 \
		    | sed -e 's/.*$(SP_PAT)[^=:]*[=:] *//' > $$f-$(PATTERN);\
		echo $$f-$(PATTERN); \
	    fi; \
	done;
	touch $@

_res/aggr_eq_err/%:
	@mkdir -p `dirname $@`
	$(eval SUFFIX:=$(call uucut,1))
	$(eval PATTERN:=$(call uucut,2))
	for s in $(SELS); do \
	    if [ "$$s" = "--sep--" ]; then \
		:; \
	    else \
		fbase=$$(echo $(OUTS) | tr " " "\012" | grep -e $$s); \
		f=`ls -t $${fbase}-*_$(SUFFIX).err 2>/dev/null \
		    | head -n 1 | sed -e 's/.err$$//'`; \
		grep '$(PATTERN) *=' $$f.err 2>/dev/null | tail -n 1 \
		    | sed -e 's/.*$(PATTERN) *= *//' > $$f-$(PATTERN);\
		touch $$f-$(PATTERN).err; \
		echo $$f-$(PATTERN); \
	    fi; \
	done;
	touch $@

_res/aggr/%-totvisits:
	@mkdir -p `dirname $@`
	$(eval SUFFIX:=$*)
	for s in $(SELS); do \
	    if [ "$$s" = "--sep--" ]; then \
		:; \
	    else \
		fbase=$$(echo $(OUTS) | tr " " "\012" | grep -e $$s); \
		f=`ls -t $${fbase}-*_$(SUFFIX).err 2>/dev/null \
		    | head -n 1 | sed -e 's/.err$$//'`; \
		grep 'nb_visits_total=' $$f.err 2>/dev/null \
		    | tail -n 1 | sed -e 's/.*= *//' > $$f-totvisits; \
		echo $$f-totvisits; \
	    fi; \
	done;
	touch $@

_res/aggr/%-nshat:
	@mkdir -p `dirname $@`
	$(eval SUFFIX:=$*)
	for s in $(SELS); do \
	    if [ "$$s" = "--sep--" ]; then \
		:; \
	    else \
		fbase=$$(echo $(OUTS) | tr " " "\012" | grep -e $$s); \
		f=`ls -t $${fbase}-*_$(SUFFIX).err 2>/dev/null \
		    | head -n 1 | sed -e 's/.err$$//'`; \
		grep 'nb_shattered=' $$f.err 2>/dev/null \
		    | tail -n 1 | sed -e 's/.*= *//' > $$f-nshat; \
		echo $$f-nshat; \
	    fi; \
	done
	touch $@

_res/aggr/%-n-m-maxdeg:
	@mkdir -p `dirname $@`
	$(eval SUFFIX:=$*)
	for s in $(SELS); do \
    if [ "$$s" = "--sep--" ]; then \
		:; \
	    else \
		fbase=$$(echo $(OUTS) | tr " " "\012" | grep -e $$s); \
		f=`ls -t $${fbase}-*_$(SUFFIX).err 2>/dev/null \
		    | head -n 1 | sed -e 's/.err$$//'`; \
		if grep "^is simple: 0" $$f.err >/dev/null 2>&1; then \
		 nm=$$(grep 'm=' $$f.err 2>/dev/null | head -n 2 | tail -n 1); \
		 degs=$$(grep 'degrees:' $$f.err 2>/dev/null | head -n 2 | tail -n 1); \
		else \
		 nm=$$(grep 'm=' $$f.err 2>/dev/null | head -n 1); \
		 degs=$$(grep 'degrees:' $$f.err 2>/dev/null | head -n 1); \
		fi; \
		echo "$$nm" \
		| sed -e 's/n=//' -e 's/m=//' -e 's/ isol.*=/ /' \
		| awk '{ \
	n = $$1; m = $$2; if (NF > 2 && 0) n = $$1 - $$3; \
	printf "%d %d ", n, m \
  }' > $$f-n-m-maxdeg; \
		echo "$$degs" \
		| sed -e 's/.* max: *//' -e 's/ .*//' >> $$f-n-m-maxdeg; \
		echo $$f-n-m-maxdeg; \
	    fi; \
	done
	touch $@



_res/selres.csv: _res/aggr/vcdimopt_1_0_0_1_0_1_64-n-m-maxdeg
	$(eval COLS:=vcdimopt_1_0_0_1_0_1_64-n-m-maxdeg \
		vcdimopt_1_0_0_1_0_1_64 vcdimopt_1_0_0_1_0_1_64-time)
	echo "graph,n,m,maxdeg,VCdim,time" > $@;
	for s in $(SELS); do \
	    if [ "$$s" = "--sep--" ]; then \
		:; \
	    else \
		: /bin/echo "$$s" | $(SHORTNAME); \
		/bin/echo -n "$$s" | $(SHORTNAME) >> $@; \
		fbase=$$(echo $(OUTS) | tr " " "\012" | grep -e $$s|head -n 1);\
		for suffix in $(COLS); do \
		    val=$$(cat $${fbase}-*_$${suffix} 2>/dev/null | tail -n 1);\
		    /bin/echo -n ",$$val" | tr " " "," >> $@; \
		done; \
		echo "" >> $@; \
	    fi; \
	done

selres2: _res/aggr/vcdimopt_1_0_0_1_0_1_64-n-m-maxdeg \
		_res/aggr/vcdimopt_1_0_0_1_0_1_64-time
	$(eval COLS:=vcdimopt_1_0_0_1_0_1_64-n-m-maxdeg \
		vcdimopt_1_0_0_1_0_1_64 vcdimopt_1_0_0_1_0_1_64-time)
	for s in $(SELS); do \
	    if [ "$$s" = "--sep--" ]; then \
	        echo "\hline"; \
	    else \
		: /bin/echo "$$s" | $(SHORTNAME); \
		/bin/echo -n "$$s" | $(SHORTNAME); \
		fbase=$$(echo $(OUTS) | tr " " "\012" | grep -e $$s|head -n 1);\
		for suffix in $(COLS); do \
		    val=$$(cat $${fbase}-*_$${suffix} 2>/dev/null | tail -n 1);\
		    /bin/echo -n ",$$val" | tr " " "," \
		    | sed -e 's/,/ \& /g' | tr -d "\012"; \
		done; \
		echo "\\\\"; \
	    fi; \
	done

selres3: _res/aggr/vcdimopt_1_0_0_1_0_1_64-n-m-maxdeg \
		_res/aggr/vcdimopt_1_0_0_1_0_1_64-time \
		_res/aggr_eq/vcdimopt_1_0_0_1_0_1_64__Maximum_resident_set_size
	$(eval COLS:=vcdimopt_1_0_0_1_0_1_64-n-m-maxdeg \
		vcdimopt_1_0_0_1_0_1_64 vcdimopt_1_0_0_1_0_1_64-time \
		vcdimopt_1_0_0_1_0_1_64-Maximum_resident_set_size)
	for s in $(SELS); do \
	    if [ "$$s" = "--sep--" ]; then \
	        echo "\hline"; \
	    else \
		: /bin/echo "$$s" | $(SHORTNAME); \
		/bin/echo -n "$$s" | $(SHORTNAME); \
		fbase=$$(echo $(OUTS) | tr " " "\012" | grep -e $$s|head -n 1);\
		for suffix in $(COLS); do \
		    val=$$(cat $${fbase}-*_$${suffix} 2>/dev/null | tail -n 1);\
		    /bin/echo -n ",$$val" | tr " " "," \
		    | sed -e 's/,/ \& /g' | tr -d "\012"; \
		done; \
		echo "\\\\"; \
	    fi; \
	done

_res/nshat.csv: _res/aggr/vcdimopt_1_0_0_1_0_1_64-time \
		_res/aggr/vcdimopt_1_0_0_1_0_0_64-time \
		_res/aggr/vcdimopt_1_0_0_0_0_1_64-time \
		_res/aggr/read-time \
		_res/aggr/nb-shattered-visited-highdeg \
		_res/aggr/nb-shattered-all-nshat \
		_res/aggr/nb-shattered-visited-totvisits \
		_res/aggr/vcdimopt_1_0_0_1_0_1_64-nshat \
		_res/aggr/vcdimopt_1_0_0_1_0_1_64-totvisits \
		_res/aggr/vcdimopt_1_0_0_0_0_1_64-totvisits \
		_res/aggr_eq/vcdimopt_1_0_0_1_0_1_64__nb_root_balls \
		_res/aggr_eq/vcdimopt_1_0_0_0_0_1_64__nb_root_balls \
		_res/aggr_eq/vcdim__avg_degree \
		_res/aggr_eq/vcdimopt_1_0_0_1_0_1_64__avg_ball_size
	$(eval COLS:=vcdimopt_1_0_0_1_0_1_64 \
		nb-shattered-all-nshat nb-shattered-highdeg \
		nb-shattered-visited \
		nb-shattered-visited-totvisits \
		nb-shattered-visited-highdeg \
		read-time vcdimopt_1_0_0_1_0_1_64-time \
		vcdimopt_1_0_0_1_0_0_64-time \
		vcdimopt_1_0_0_0_0_1_64-time \
		vcdimopt_1_0_0_1_0_1_64-nshat \
		vcdimopt_1_0_0_1_0_1_64-totvisits\
		vcdimopt_1_0_0_0_0_1_64-totvisits\
		vcdimopt_1_0_0_1_0_1_64-nb_root_balls \
		vcdimopt_1_0_0_0_0_1_64-nb_root_balls \
		vcdim-avg_degree \
		vcdimopt_1_0_0_1_0_1_64-avg_ball_size)
	echo "graph,VCdim,nshat-all,nshat-hd,nshat-vis,ntot-vis,nhighdeg,time-read,time-kbg,time-kg,time-kb,kbg-vis,kbg-tot,kb-tot,kbg-nhdeg,kb-nhdeg,kbg-deg,kbg-bsize" > $@;
	for s in $(SELS); do \
	    if [ "$$s" = "--sep--" ]; then \
		:; \
	    else \
		: /bin/echo "$$s" | $(SHORTNAME); \
		fbase=$$(echo $(OUTS) | tr " " "\012" | grep -e $$s|head -n 1);\
		/bin/echo -n "$$s" | $(SHORTNAME) >> $@; \
		for suffix in $(COLS); do \
		    val=$$(cat $${fbase}-*_$${suffix} 2>/dev/null | tail -n 1);\
		    /bin/echo -n ",$$val" >> $@; \
		done; \
		echo "" >> $@; \
	    fi; \
	done

_res/nshat-sort.csv: _res/nshat.csv
	head -n 1 $< > $@
	cat $< | grep -v 'graph,' | sort -t , -k 2 -n -r >> $@

_res/nshat-deg.csv: _res/nshat.csv
	head -n 1 $< > $@
	cat $< | grep -v -e 'graph,' -e ',-nan,' -e ',,$$' \
		| sort -t , -k 2 -n -r >> $@

_res/nshat.pdf: tools/plot-nshat.py _res/nshat-sort.csv 
	python3 $^ nhighdeg nshat-vis $@

_res/nshat-hd.pdf: tools/plot-nshat-hd.py _res/nshat-sort.csv 
	python3 $^ nhighdeg nshat-hd $@

_res/nshat-compl.pdf: tools/plot-nshat-compl.py _res/nshat-deg.csv 
	python3 $^ prod-bsize time-kbg kbg-nhdeg $@

_res/nshat-lem1.pdf: tools/plot-nshat-lem1.py _res/nshat-sort.csv 
	python3 $^ nshat-hd nshat-vis nhighdeg $@

%.match:
	@mkdir -p /tmp/$(ME)
	$(eval TMPO:=$(shell mktemp -u /tmp/$(ME)/_resO.XXXX))
	$(eval TMPA:=$(shell mktemp -u /tmp/$(ME)/_resA.XXXX))
	$(eval TMPM:=$(shell mktemp -u /tmp/$(ME)/_resM.XXXX))
	@$(MAKE) -s res ARGS=$* \
	| sed -e 's/-[0-9]*h_$*/ $*/' | grep -e '^1_done' > $(TMPO)
	@$(MAKE) -s res \
	| sed -e 's/-[0-9]*h_$(SUFFIX)/ $(SUFFIX)/' | grep -e '^1_done'> $(TMPA)
	@cat $(TMPO) $(TMPA) | cut -d' ' -f2,3,6- | sort | $(AWK_MERGE) \
	| sed -e 's/ *isolated_nodes=[0-9]*//' -e 's/  */ /g' -e 's/_ / /g' \
		-e 's/ _/ /g' -e 's|^_res[^ ]*/||' -e 's/.edgelist / /' \
		-e 's/.txt / /' > $(TMPM)
	@cat $(TMPM) | awk '{ if ($$4 <= $$12) \
		print $$1,$$2,$$10,$$3,$$4,$$12,$$5,$$6,$$14,$$7,$$8; \
		if ($$4 < $$12) { name_a=$$2; win_a+=1; } \
		if ($$4 == $$12) { eq_a+=1; } \
	  } END{printf "%s %d wins %deq 1000000000\n", name_a, win_a, eq_a }' \
	  | sort -n -k 5
	@echo
	@cat $(TMPM) | awk '{ if ($$4 > $$12) \
		print $$1,$$2,$$10,$$3,$$4,$$12,$$5,$$6,$$14,$$7,$$8; \
		name_a=$$2; name_b=$$10; if ($$4 < $$12) win_a+=1; \
		if ($$12 < $$4) win_b+=1; \
		if ($$4 == $$12) { eq_a+=1; } \
	  } END{printf "%s %d wins vs %s 1000000000 %d wins  %d eq\n", \
		name_a, win_a, name_b, win_b, eq_a }' | sort -n -k 6
	@echo
	@echo `$(MAKE) -s $(TMPM).first COL=2` `$(MAKE) -s $(TMPM).sum COL=4` \
	    vs `$(MAKE) -s $(TMPM).first COL=10` `$(MAKE) -s $(TMPM).sum COL=12`
	@rm -f $(TMPO) $(TMPA) $(TMPM)

cleanres%:
	@for fbase in $(OUTS); do \
		f=$$fbase$*; if [ "$*" == ".empty" ]; then f=$$fbase; fi; \
		rm -f $$f $$f.err $$f.env; \
	done




rsleep:
	rand=`bash -c 'echo $$RANDOM'`; sleep $$(( rand % 200 ))
	@echo "wake up"





# ---------- slurm cluster stuff -----------------


%.ssh: _scp
	ssh $(CLUST) 'cd $(CLUST_DIR); $(MAKE) $* \
		      WALLTIME=$(WALLTIME) PARTITION=$(PARTITION) \
                      ARGS="$(ARGS)" TIMEOUT=$(TIMEOUT) WORKER=$(WORKER)'

%.get:
	scp $(CLUST):$(CLUST_DIR)/$* ./

# examples :
# make do_one.batch.ssh ARGS="graphs/more/massive/froz-w.bcc.txt -a 12 -domd 42 -domr 1.1"
# make do_many_targets.batch.ssh ARGS="-algo 2" TIMEOUT=24h WORKER=4

WORKER:=$(if $(WORKER),$(WORKER),1)
PARTITION:=cpu_homogen

%.batch:
	for worker in {1..$(WORKER)}; do \
		fname=$*-$$worker-$(TIMEOUT)_$(SUFFIX); \
		$(MAKE) batch/$$fname.launch FNAME=$$fname \
                      PARTITION=$(PARTITION) TARGET=$* \
		      ARGS="$(ARGS)" TIMEOUT=$(TIMEOUT); \
	done
	tail -n 3 batch/$*-*-$(TIMEOUT)_$(SUFFIX).out

batch/%.launch:
	mkdir -p batch
	$(eval SCRIPT:=batch/$(FNAME).sh)
	$(eval OUT:=batch/$(FNAME).out)
	$(eval ERR:=batch/$(FNAME).err)
	$(eval WTM:=--time=$(if $(WALLTIME),$(WALLTIME),48:00:00) \
		    --mem=50G) \
	echo "#!/bin/bash" > $(SCRIPT)
	echo 'echo SLURM_JOB_ID: $${SLURM_JOB_ID} > $(ERR)' >> $(SCRIPT)
	echo 'echo SLURM_JOB_NAME: $${SLURM_JOB_NAME} >> $(ERR)' >> $(SCRIPT)
	echo 'echo SLURM_JOB_NODELIST: $${SLURM_JOB_NODELIST} >> $(ERR)' \
		>> $(SCRIPT)
	echo 'echo partition $(PARTITION)' >> $(SCRIPT)
	echo '$(MODULES)' >> $(SCRIPT)
	echo '$(MAKE) $(TARGET) ARGS="$(ARGS)" \
		TIMEOUT=$(TIMEOUT) > $(OUT) 2>> $(ERR)' >> $(SCRIPT)
	chmod 755 $(SCRIPT)
	sbatch $(WTM) $(if $(PARTITION),-p $(PARTITION),) $(SCRIPT) | tee $@
	@echo '(sbatch $(WTM) $(if $(PARTITION),-p $(PARTITION),) $(SCRIPT))' >> $@
	@while [ ! -f $(OUT) ]; do echo -n "."; sleep 1; done
	@echo; echo; echo " --- $*: $$(< $@ ) STDOUT :"; cat $(OUT)
	@sleep 10; echo; echo; echo " --- $*: $$(< $@ ) STDERR :"; cat $(ERR)
	@slurmfile=slurm-`cat $@ | awk '{print($$4)}'`.out; \
	if [ -f "$$slurmfile" ]; then rm $$slurmfile; fi
	sleep 10

%.batch12:
	$(MAKE) _batch12/$*-$(TIMEOUT)_$(SUFFIX).launch12 \
                      PARTITION=$(PARTITION) TARGET=$* \
		      ARGS="$(ARGS)" TIMEOUT=$(TIMEOUT)
	tail -n 3 _batch12/$*-$(TIMEOUT)_$(SUFFIX).out

_batch12/%.launch12:
	mkdir -p _batch12
	$(eval SCRIPT:=_batch12/$*.sh)
	$(eval OUT:=_batch12/$*.out)
	$(eval ERR:=_batch12/$*.err)
	$(eval WTM:=--time=$(if $(WALLTIME),$(WALLTIME),48:00:00) \
		    --cpus-per-task=12)
	echo "#!/bin/bash" > $(SCRIPT)
	echo 'echo SLURM_JOB_ID: $${SLURM_JOB_ID} > $(ERR)' >> $(SCRIPT)
	echo 'echo SLURM_JOB_NAME: $${SLURM_JOB_NAME} >> $(ERR)' >> $(SCRIPT)
	echo 'echo SLURM_JOB_NODELIST: $${SLURM_JOB_NODELIST} >> $(ERR)' \
		>> $(SCRIPT)
	echo 'echo partition $(PARTITION)' >> $(SCRIPT)
	echo '$(MODULES)' >> $(SCRIPT)
	echo '$(MAKE) -j 12 $(TARGET) ARGS="$(ARGS)" \
		TIMEOUT=$(TIMEOUT) > $(OUT) 2>> $(ERR)' >> $(SCRIPT)
	chmod 755 $(SCRIPT)
	sbatch $(WTM) $(if $(PARTITION),-p $(PARTITION),) $(SCRIPT) | tee $@
	@echo '(sbatch $(WTM) $(if $(PARTITION),-p $(PARTITION),) $(SCRIPT))' >> $@
	@while [ ! -f $(OUT) ]; do echo -n "."; sleep 1; done
	@echo; echo; echo " --- $$(< $@ ) STDOUT :"; cat $(OUT)
	@sleep 10; echo; echo; echo " --- $$(< $@ ) STDERR :"; cat $(ERR)
	@slurmfile=slurm-`head -n 1 $@ | awk '{print($$4)}'`.out; \
	if [ -f "$$slurmfile" ]; then rm $$slurmfile; fi; echo; echo


jobs:
	squeue -l -u $(ME)

%.control:
	scontrol show job $*

%.cancel:
	scancel $*

%.connect:
	srun --jobid $* --overlap --pty bash

partitions:
	sinfo -a

cpuinfo: hostinfo
	cat /proc/cpuinfo

info_cpu_mem:
	uname -a
	$(eval SYST:=$(shell uname))
	$(if $(filter Darwin,$(SYST)),:,lscpu)
	$(if $(filter Darwin,$(SYST)),:,free -g)


# --------- misc --------------

%.cat: %
	@cat $<

%.head: %
	@head -n 20 $<

%.tail: %
	@tail -n 20 $<

%.wc: %
	@wc $<

%.lines: %
	@wc -l $< | awk '{print($$1);}'

%.force:
	@rm -f $*; $(MAKE) $*

%.open:
	$(MAKE) $*; open $*

%.forceopen:
	@rm -f $*; $(MAKE) $*; open $*

%.rm:
	rm -i $*

hello:
	echo hello

uucut=$(word $(1),$(subst __, ,$*))
# example : %.swap: echo $(call uucut,2)__$(call uucut,1)

# produce an error if some variables are not defined:

get = $($(1))

define require1
	if [ -z "$(call get,$(1))" ]; then \
		echo "Makefile variable $(1) undefined!"; \
		exit 1;\
	fi
endef

require = $(if $(1),$(call require1,$(word 1, $(1))); \
                    $(call require,$(wordlist 2,$(words $(1)),$(1))),)

trequire:
	$(call require, A B)
	echo sequel...

%.first:
	$(eval COL:=$(if $(COL),$(COL),1))
	@head -n 1 $* | cut -d' ' -f$(COL)

%.sum:
	$(eval COL:=$(if $(COL),$(COL),1))
	@cat $* | $(AWK_SUM)

AWK_SUM=awk -v c=$(COL) 'BEGIN{ sum = 0.0 } { sum += $$c } END{ print sum }'
AWK_MERGE:= awk 'BEGIN{ prev=""; line="" } { if ($$1 == prev) print(line, $$0); line=$$0; prev=$$1; }'
